<?php require_once '../app/views/partials/header.php'; ?>
<body class="hold-transition sidebar-mini  layout-fixed">
    <section class="content" style="padding: 150px 0;">
        <div class="row">
            <div class="col-md-12 text-center">
                <img src="/img/logo.png" width="30%" />
            </div>
        </div>
        <div class="error-page">
            <h2 class="headline text-warning"> 404</h2>

            <div class="error-content">
                <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Página não encontrada.</h3>

                <p>
                Não foi possível encontrar a página que você procurava.
                Enquanto isso, você pode <a href="/dashboard">retornar para a dashboard</a>.
                </p>
            </div>
        </div>
    </section>
<?php require_once '../app/views/partials/footer.php'; ?>
</body>
</html>