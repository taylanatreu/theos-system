<?php require_once '../app/views/partials/header.php'; ?>
<body class="hold-transition sidebar-mini  layout-fixed">
    <section class="content" style="padding: 150px 0;">
        <div class="row">
            <div class="col-md-12 text-center">
                <img src="/img/logo.png" width="30%" />
            </div>
        </div>
        <div class="error-page">
            <h2 class="headline text-danger"> 402</h2>

            <div class="error-content">
                <h3><i class="fas fa-ban text-danger"></i> Usuário não autorizado!</h3>

                <p>
                Você não possui permissão para acessar essa página
                Entre em contato com o administrador do sistema, ou então <a href="/dashboard">retorne para a dashboard</a>.
                </p>
            </div>
        </div>
    </section>
<?php require_once '../app/views/partials/footer.php'; ?>
</body>
</html>