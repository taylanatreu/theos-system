<?php 
require '../app/views/partials/header.php';
require '../app/views/partials/session.php';
?>
<body class="hold-transition sidebar-mini  layout-fixed">
    <div class="wrapper">
    <?php 
    include "../app/views/partials/navbar.php";
    include "../app/views/partials/sidebar-menu.php" 
    ?>

    <!-- CONTEUDO DA PAGINA -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"><i class="fas fa-users-cog"></i> Usuários</h1>
                        <small>Gerencie os dados dos usuários com acesso ao sistema.</small>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard" class="link">Dashboard</a></li>
                        <li class="breadcrumb-item active">Usuários</li>
                        </ol>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <button type="button" class="btn btn-success btn-xs" id="novoCadastro" title="Novo Usuário"><i class="fas fa-plus-circle"></i> Novo Usuário</button>
            </div>
        </div>
        
        <section class="content">
            <div class="container-fluid">

                <!-- CADASTRO DE USUÁRIOS -->
                <div id="cardCadastro" style="display: none;">
                    <?php include "../app/views/usuarios/forms/form-cadastro.php"; ?>
                </div>
                
                <!-- LISTA DE USUÁRIOS -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fa fa-search"></i> Lista de Usuários</h3>

                        <div class="card-tools">
                            <?php include "../app/views/partials/busca-simples.php" ?>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <div class="overlay-wrapper">
                            <?php include "../app/views/partials/loading.php"; ?>

                            <table class="table table-hover table-striped text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">IMAGEM</th>
                                        <th>NOME</th>
                                        <th class="text-center">EMAIL</th>
                                        <th class="text-center">STATUS</th>
                                        <th class="text-center">AÇÕES</th>
                                    </tr>
                                </thead>
                                <tbody id="registros">
                                    <?php 
                                    $x = 1;
                                    if (isset($users) && is_array($users) && !empty($users)) {
                                    foreach($users as $usuario) { 
                                        ?>
                                    <tr>
                                        <td class="text-center" style="width: 10%;"><?=$x?></td>
                                        <td class="text-center">
                                            <div>
                                                <?php
                                                if ($usuario->avatar) {
                                                ?>
                                                <img 
                                                    class="img-fluid img-circle"
                                                    width="50"
                                                    src="/storage/users/<?=$usuario->avatar?>"
                                                    alt="<?=$usuario->nome?> <?=$usuario->sobrenome?>">
                                                <?php } else {?>
                                                    <i class="fas fa-user-circle fa-2x"></i>
                                                <?php } ?>
                                            </div>
                                            
                                        </td>
                                        <td>
                                            <div>
                                                <strong><?=$usuario->nome?> <?=$usuario->sobrenome?></strong>
                                            </div>
                                        </td>
                                        <td class="text-center"><?=$usuario->email?></td>
                                        <td class="text-center">
                                            <?php if ($usuario->ativo == 1) { ?>
                                                <i class="far fa-check-square"></i>
                                            <?php } else { ?>
                                                <i class="fas fa-ban"></i>
                                            <?php } ?>
                                        </td>
                                        <td class="text-center">
                                            <button 
                                                type="button" 
                                                class="btn btn-xs btn-info" 
                                                title="Editar Usuário" 
                                                onclick="editarRegistro(<?=$usuario->id?>)">
                                                <i class="fas fa-edit"></i>
                                            </button> 

                                            <?php if ($usuario->ativo == 1) { ?>
                                                <a 
                                                    href="#" 
                                                    class="btn btn-xs btn-warning" 
                                                    data-toggle="modal" 
                                                    data-target="#confirmacaoDesabilitaModal" 
                                                    data-id="<?=$usuario->id?>"
                                                    title="Desabilitar Usuário">
                                                    <i class="fas fa-ban"></i>
                                                </a>
                                            <?php } else { ?>
                                                <a 
                                                    href="#" 
                                                    class="btn btn-xs btn-success" 
                                                    data-toggle="modal" 
                                                    data-target="#confirmacaoHabilitaModal" 
                                                    data-id="<?=$usuario->id?>" 
                                                    title="Habilitar Usuário">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php $x ++; } } else { ?>
                                        <tr>
                                            <td class="text-center" colspan="6">Nenhum registro encontrado.</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <?php include "../app/views/partials/paginacao.php"; ?>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <?php include "../app/views/partials/copyright.php" ?>
    </div>

    <?php include "../app/views/partials/modals.php"; ?>

<?php require '../app/views/partials/footer.php'; ?>

</body>
</html>