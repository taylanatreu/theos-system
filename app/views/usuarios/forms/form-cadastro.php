<div class="card">
    <div class="card-header">
        <h3 class="card-title"><i class="fas fa-plus-circle"></i> Cadastro do Usuário</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" id="closeBtn">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <form id="formCadastro">
        <input name="registroId" id="registroId" type="hidden" value="">
        <input name="grupoId" id="grupoId" type="hidden" value=""> 
        <div class="card-body">
        <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input name="nome" type="text" class="form-control" id="nome" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="sobrenome">Sobrenome</label>
                        <input name="sobrenome" type="text" class="form-control" id="sobrenome" required>
                    </div>
                </div>
                <div class="col-md-4">                    
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input name="email" type="email" class="form-control" id="email" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="senhaa">Senha</label>
                        <div class="input-group">
                            <input class="form-control" type="password" id="senhaa" name="senha" required>
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="mostrarSenhaBtn">
                                    <i class="far fa-eye"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="confirmaSenha">Confirme a senha</label>
                        <div class="input-group">
                            <input class="form-control" type="password" id="confirmaSenha" name="confirmaSenha" required>
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="mostrarConfirmaSenhaBtn">
                                    <i class="far fa-eye"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">                    
                    <div class="form-group">                                                
                        <label for="checkboxPrimary1">Ativo</label><br />
                        <div class="icheck-success d-inline">
                            <input name="ativo" value="1" type="checkbox" id="checkboxPrimary1" checked="">
                            <label for="checkboxPrimary1">
                            </label>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="form-group text-right">
                <button type="button" class="btn btn-default" onclick="limparFormCadastro('formCadastro')"><i class="fas fa-eraser"></i> Limpar</button>
                <button type="submit" class="btn btn-primary" id="btnSalvar"><i class="fas fa-save"></i> Salvar</button>
            </div>
        </div>
    </form>
</div>