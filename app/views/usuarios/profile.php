<?php 
require '../app/views/partials/header.php';
require '../app/views/partials/session.php';
?>
<body class="hold-transition sidebar-mini  layout-fixed">
    <div class="wrapper">
    <?php 
    include "../app/views/partials/navbar.php";
    include "../app/views/partials/sidebar-menu.php" 
    ?>

    <!-- CONTEUDO DA PAGINA -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"><i class="fas fa-user"></i> Meu Perfil</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard" class="link">Dashboard</a></li>
                        <li class="breadcrumb-item active">Meu Perfil</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-orange card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <?php
                                    if ($pegaUsuario->avatar) {
                                    ?>
                                    <img 
                                        class="profile-user-img img-fluid img-circle"
                                        src="/storage/users/<?=$pegaUsuario->avatar?>"
                                        alt="<?=$pegaUsuario->nome?> <?=$pegaUsuario->sobrenome?>"
                                        id="avatarImg"
                                        >
                                    <?php } else {?>
                                        <i class="fas fa-user-circle fa-4x"></i>
                                    <?php } ?>
                                </div>
                                <h3 class="profile-username text-center">
                                    <strong><span id="infoProfile"><?=$pegaUsuario->nome?> <?=$pegaUsuario->sobrenome?></span> (<?=$pegaUsuario->login?>)</strong>    
                                </h3>
                                <p class="text-muted text-center"><span id="emailProfile"><?=$pegaUsuario->email?></span></p>
                                <div class="text-muted text-center">Criado em: <?=date("d/m/Y H:i", strtotime($pegaUsuario->created_at));?></div>
                                
                                <?php if ($pegaUsuario->updated_at) { ?>
                                    <div class="text-muted text-center">Última alteração: <span id="updatedProfile"><?=date("d/m/Y H:i", strtotime($pegaUsuario->updated_at));?></span></div>
                                <?php } ?>
                            </div>
                        </div>

                        <?php
                        if ($gruposUsuario) { 
                        ?>
                        <div class="card card-orange card-outline">
                            <div class="card-body box-profile">
                                <h3 class="profile-username text-center">
                                    <strong>Meus Grupos de Acesso</strong>    
                                </h3>
                                <p class="text-muted text-center">
                                    <?php
                                    foreach ($gruposUsuario as $grupo) {
                                    ?>
                                        <span class="badge badge-dark"><?=$grupo->name?></span>
                                    <?php } ?>

                                </p>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header p-2">
                                <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active" href="#dados" data-toggle="tab"><i class="fas fa-edit"></i> Meus Dados</a></li>
                                <li class="nav-item"><a class="nav-link" href="#avatar" data-toggle="tab"><i class="fas fa-camera"></i> Imagem de Avatar</a></li>
                                <li class="nav-item"><a class="nav-link" href="#senha" data-toggle="tab"><i class="fas fa-key"></i> Alterar Senha</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="overlay-wrapper">     
                                    <?php include "../app/views/partials/loading.php"; ?>
                                    <div class="tab-content">
                                        <div class="active tab-pane" id="dados">
                                            <form id="formProfile">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nome">Nome</label>
                                                            <input name="nome" type="text" class="form-control" id="nome" value="<?=$pegaUsuario->nome?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="sobrenome">Sobrenome</label>
                                                            <input name="sobrenome" type="text" class="form-control" id="sobrenome" value="<?=$pegaUsuario->sobrenome?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">Email</label>
                                                            <input name="email" type="email" class="form-control" id="email" value="<?=$pegaUsuario->email?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="login">Login</label>
                                                            <input name="login" type="text" class="form-control" id="login" value="<?=$pegaUsuario->login?>" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="tab-pane" id="avatar">
                                            <form id="formAvatar" enctype="multipart/form-data">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="imagem">Imagem</label>
                                                            <input name="imagem" type="file" class="form-control" id="imagem" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="tab-pane" id="senha">

                                            <form id="formSenha">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="senhaa">Senha</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="password" id="senhaa" name="senha" required>
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-outline-secondary" type="button" id="mostrarSenhaBtn">
                                                                        <i class="far fa-eye"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="confirmaSenha">Confirme a senha</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="password" id="confirmaSenha" name="confirmaSenha" required>
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-outline-secondary" type="button" id="mostrarConfirmaSenhaBtn">
                                                                        <i class="far fa-eye"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php include "../app/views/partials/copyright.php" ?>
    </div>

<?php require '../app/views/partials/footer.php'; ?>

</body>
</html>