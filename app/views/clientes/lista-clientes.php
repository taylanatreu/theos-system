<?php 
require '../app/views/partials/header.php';
require '../app/views/partials/session.php';
?>
<body class="hold-transition sidebar-mini  layout-fixed">
    <div class="wrapper">
    <?php 
    include "../app/views/partials/navbar.php";
    include "../app/views/partials/sidebar-menu.php" 
    ?>

    <!-- CONTEUDO DA PAGINA -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"><i class="fas fa-users"></i> Clientes</h1>
                        <small>Gerencie os dados e faça a gestão dos seus clientes.</small>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard" class="link">Dashboard</a></li>
                        <li class="breadcrumb-item active">Clientes</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        
        <section class="content">
            <div class="container-fluid">

                <div id="resultadoPesquisa"></div>

                <!-- LISTA DE CLIENTES -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fa fa-search"></i> Lista de Clientes</h3>

                        <div class="card-tools">
                            <a href="#" class="btn btn-xs btn-default" title="Filtro" data-toggle="modal" data-target="#filtroPesquisaModal">
                                <i class="fas fa-filter"></i> Filtro
                            </a>
                            <a href="/clientes/create" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="Novo Registro">
                                <i class="fas fa-plus-circle"></i> Novo
                            </a>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <div class="overlay-wrapper">
                            <?php include "../app/views/partials/loading.php"; ?>

                            <table class="table table-hover table-striped text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 10%;">ID</th>
                                        <th style="width: 25%;">CLIENTE</th>
                                        <th style="width: 25%;">CONTATOS</th>
                                        <th style="width: 10%;"><i class="fas fa-calendar-alt"></i></th>
                                        <th class="text-center" style="width: 10%;">STATUS</th>
                                        <th class="text-center" style="width:20%;">AÇÕES</th>
                                    </tr>
                                </thead>
                                <tbody id="registros">
                                <?php 
                                    $x = 1;
                                    if (isset($clientes) && is_array($clientes) && !empty($clientes)) {
                                    foreach($clientes as $cliente) { 
                                        if ($cliente->tipoCliente == 1) {
                                            $descCpf = "CPF";
                                        } else {
                                            $descCpf = "CNPJ";
                                        }   

                                        if ($cliente->tipoContato == 1) {
                                            $icon = "fab fa-whatsapp";
                                        } else {
                                            $icon = "fas fa-mobile-alt";
                                        }

                                    ?>
                                    <tr>
                                        <td class="text-center"><?=$x?></td>
                                        <td class="text-center">
                                            <a href="/clientes/details?clienteId=<?=$cliente->id?>" class="link">
                                                <?php
                                                if ($cliente->avatar) {
                                                ?>
                                                    <img 
                                                        class="img-fluid"
                                                        width="150"
                                                        src="/storage/users/<?=$cliente->avatar?>"
                                                        alt="<?=$cliente->nome?> <?=$cliente->sobrenome?>">
                                                <?php } else { ?>
                                                    <i class="fa fa-user fa-3x"></i>
                                                <?php } ?>
                                            </a>
                                        </td>
                                        <td>
                                            <div><strong><?=$cliente->nome?> <?=$cliente->sobrenome?></strong></div>
                                            <div class="text-muted"><strong><?=$descCpf?>:</strong> <span id="cpfCnpj"><?=$cliente->cpfCnpj?></span></div>
                                        </td>                                        
                                        <td>
                                        <div><i class="fas fa-envelope"></i> <strong><?=$cliente->email?></strong></div>
                                        <div><i class="<?=$icon?>"></i> <span id="celular"><?=$cliente->celular?></span></div>
                                                                                        
                                        </td>
                                        <td class="text-center">
                                            <?php if ($cliente->ativo == 1) { ?>
                                                <div class="badge badge-success">Ativo</div>
                                            <?php } else { ?>
                                                <div class="badge badge-dark">Desabilitado</div>
                                            <?php } ?>
                                        </td>
                                        <td class="text-center">
                                            <a href="/clientes/details?clienteId=<?=$cliente->id?>" class="btn btn-xs btn-dark link" data-toggle="tooltip" data-placement="top" title="Detalhes do Cliente">
                                                <i class="fas fa-eye"></i>
                                            </a>

                                            <a href="/clientes/create?clienteId=<?=$cliente->id?>" class="btn btn-xs btn-info link" data-toggle="tooltip" data-placement="top" title="Editar Cliente">
                                                <i class="fas fa-edit"></i>
                                            </a>

                                            <?php if ($cliente->ativo == 1) { ?>
                                                <a 
                                                    href="#" 
                                                    class="btn btn-xs btn-warning" 
                                                    data-toggle="modal" 
                                                    data-target="#confirmacaoDesabilitaModal" 
                                                    data-id="<?=$cliente->id?>"
                                                    title="Desabilitar Cliente">
                                                    <i class="fas fa-ban"></i>
                                                </a>
                                            <?php } else { ?>
                                                <a 
                                                    href="#" 
                                                    class="btn btn-xs btn-success" 
                                                    data-toggle="modal" 
                                                    data-target="#confirmacaoHabilitaModal" 
                                                    data-id="<?=$cliente->id?>" 
                                                    title="Habilitar Cliente">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php $x ++; } } else { ?>
                                        <tr>
                                            <td class="text-center" colspan="6">Nenhum registro encontrado.</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <?php include "../app/views/partials/paginacao.php"; ?>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <?php include "../app/views/partials/copyright.php" ?>
    </div>

    <?php include "../app/views/partials/modals.php"; ?>

<?php require '../app/views/partials/footer.php'; ?>

</body>
</html>