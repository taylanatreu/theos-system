<h6><strong><i class="fas fa-user"></i> Dados Pessoais</strong></h6>
<hr >

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="nome"><small>Nome Completo <span class="tooltips" data-toggle="tooltip" data-placement="top" title="Se for pessoa física digite o nome completo, se for jurídica digite o nome da empresa."><i class="fas fa-info-circle"></i></span></small></label>     
            <input type="text" id="nome" name="nome" class="form-control" value="<?=$dadosCliente->nome?> <?=$dadosCliente->sobrenome?>" required>           
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
        <label for="cpfCnpj"><small>CPF ou CNPJ <span class="tooltips" data-toggle="tooltip" data-placement="top" title="Se for pessoa física digite o CPF jurídica digite o CNPJ da empresa."><i class="fas fa-info-circle"></i></span></small></label>     
            <input type="text" id="cpfCnpj" name="cpfCnpj" class="form-control" value="<?=$dadosCliente->cpfCnpj?>" required>                  
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
        <label for="email"><small>Email <span class="tooltips" data-toggle="tooltip" data-placement="top" title="Digite um email que você tem acesso regularmente."><i class="fas fa-info-circle"></i></span></small></label>     
            <input type="email" id="email" name="email" class="form-control" autocomplete="off" value="<?=$dadosCliente->email?>" required>                  
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
        <label for="telefone"><small>Telefone  (Opcional)</small></label>     
            <input type="text" id="telefone" name="telefone" class="form-control" value="<?=$dadosCliente->telefone?>">                  
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
        <label for="celular"><small>Celular</small></label>     
            <input type="text" id="celular" name="celular" class="form-control" value="<?=$dadosCliente->email?>" required>                  
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-check pt-5">
            <input name="whatsapp" type="checkbox" value="1" id="whatsapp" class="form-check-input">
            <label for="whatsapp" class="form-check-label"><small>Esse celular é whatsapp</small></label>
        </div>
    </div>          
</div>
<hr >