
<h6><strong><i class="fas fa-key"></i> Acesso</strong></h6>
<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="senhaa"><small>Senha</small></label>
            <div class="input-group">
                <input class="form-control" type="password" id="senhaa" name="senha" required>
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" id="mostrarSenhaBtn">
                        <i class="far fa-eye"></i>
                    </button>
                </div>
            </div>
        </div>
    </div> 
    <div class="col-md-4">
        <div class="form-group">
            <label for="confirmaSenha"><small>Confirme a senha</small></label>
            <div class="input-group">
                <input class="form-control" type="password" id="confirmaSenha" name="confirmaSenha" required>
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" id="mostrarConfirmaSenhaBtn">
                        <i class="far fa-eye"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>         
</div>