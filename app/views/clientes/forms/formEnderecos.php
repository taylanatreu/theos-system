<h6><strong><i class="fas fa-home"></i> Endereço</strong></h6>
<hr >

<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="cep"><small>CEP</small></label>
            <input type="text" id="cep" name="cep" class="form-control" maxlength="9" value="<?=$dadosCliente->cep?>" required>                   
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group">
            <label for="logradouro"><small>Logradouro</small></label>
            <input type="text" id="logradouro" class="form-control" value="<?=$dadosCliente->endereco?>" readonly disabled>
            <input type="hidden" name="logradouro" id="logradouroHiden" value="<?=$dadosCliente->endereco?>" readonly>                   
        </div>
    </div> 
    <div class="col-md-2">
        <div class="form-group">
            <label for="numero"><small>Número</small></label>
            <input type="text" id="numero" name="numero" class="form-control" value="<?=$dadosCliente->numero?>"required>                   
        </div>
    </div>            
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="complemento"><small>Complemento (Opcional)</small></label>
            <input type="text" id="complemento" name="complemento" value="<?=$dadosCliente->complemento?>" class="form-control">                   
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="bairro"><small>Bairro:</small></label>
            <input type="text" id="bairro" class="form-control" value="<?=$dadosCliente->bairro?>" readonly disabled>     
            <input type="hidden" name="bairro" id="bairroHiden" value="<?=$dadosCliente->bairro?>" readonly>                                 
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="cidade"><small>Cidade:</small></label>
            <input type="text" id="cidade" class="form-control" value="<?=$dadosCliente->cidade?>" readonly disabled>                   
            <input type="hidden" name="cidade" id="cidadeHiden" value="<?=$dadosCliente->cidade?>" readonly>
        </div>
    </div> 
    <div class="col-md-2">
        <div class="form-group">
            <label for="uf"><small>UF:</small></label>
            <input type="text" id="uf" class="form-control" value="<?=$dadosCliente->uf?>" readonly disabled>                   
            <input type="hidden" name="uf" id="ufHiden" value="<?=$dadosCliente->uf?>" readonly>
        </div>
    </div>            
</div>
<hr>