<?php 
require '../app/views/partials/header.php';
require '../app/views/partials/session.php';
?>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
    <?php 
    include "../app/views/partials/navbar.php";
    include "../app/views/partials/sidebar-menu.php";
    ?>
    <!-- CONTEUDO DA PAGINA -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"><i class="fas fa-users"></i> Clientes</h1>
                        <small>Gerencie os dados e faça a gestão dos seus clientes.</small>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard" class="link">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/clientes" class="link">Clientes</a></li>
                        <li class="breadcrumb-item active">Cadastro de Cliente</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form id="formCliente">

                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-plus-circle"></i> Cadastro de Cliente</h3>
                                    <div class="card-tools">
                                        <a href="/clientes" class="btn btn-xs btn-default link"><i class="fas fa-users"></i> Lista de Clientes</a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    
                                    <div class="overlay-wrapper">     
                                        <?php include "../app/views/partials/loading.php"; ?>

                                        <?php include "../app/views/clientes/forms/form-cadastro.php"; ?>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-right">
                                        <a href="/clientes" class="btn btn-default"><i class="fas fa-users"></i> Lista de Clientes</a>
                                        <button type="submit" class="btn btn-success" id="btnSalvar" disabled><i class="fas fa-save"></i> Salvar</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php include "../app/views/partials/copyright.php" ?>
    </div>

    <?php include "../app/views/partials/modals.php"; ?>

<?php require '../app/views/partials/footer.php'; ?>

</body>
</html>