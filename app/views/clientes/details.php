<?php 
require '../app/views/partials/header.php';
require '../app/views/partials/session.php';
?>
<body class="hold-transition sidebar-mini  layout-fixed">
    <div class="wrapper">
    <?php 
    include "../app/views/partials/navbar.php";
    include "../app/views/partials/sidebar-menu.php";

    $dadosCliente = isset($cliente) ? $cliente : "";
    ?>

    <!-- CONTEUDO DA PAGINA -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0"><i class="fas fa-users"></i> Clientes</h1>
                            <small>Gerencie os dados e faça a gestão dos seus clientes.</small>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/dashboard" class="link">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="/clientes" class="link">Clientes</a></li>
                            <li class="breadcrumb-item active">Detalhes do Cliente: <strong><?=$cliente->nome?> <?=$cliente->sobrenome?></strong></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        
            <section class="content">
                <div class="container-fluid">                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-search-plus"></i> Detalhes do Cliente: <?=$cliente->nome?> <?=$cliente->sobrenome?></h3>
                                    <div class="card-tools">
                                        <a href="/clientes" class="btn btn-xs btn-default link"><i class="fas fa-university"></i> Lista de Clientes</a>
                                        <a href="/clientes/create" class="btn btn-xs btn-success link"><i class="fas fa-plus-circle"></i> Novo Cliente</a>
                                    </div>
                                </div>
                                <div class="card-body">

                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card card-orange card-outline">
                                                        <div class="card-body box-profile">
                                                            <div class="text-center">
                                                                <?php
                                                                if ($cliente->avatar) {
                                                                ?>
                                                                <img 
                                                                    class="img-fluid"
                                                                    src="/storage/users/<?=$cliente->avatar?>"
                                                                    alt="<?=$cliente->nome?> <?=$cliente->sobrenome?>"
                                                                    >
                                                                <?php } else {?>
                                                                    <i class="fas fa-user fa-4x"></i>
                                                                <?php } ?>
                                                            </div>
                                                            <h3 class="profile-username text-center">
                                                                <strong><?=$cliente->nome?> <?=$cliente->sobrenome?></strong>    
                                                            </h3>
                                                            <p class="text-muted text-center">
                                                                <?php echo $cliente->tipoCliente == 1 ? "CPF: " : "CNPJ: "; ?> <span id="cpfCnpj"><?=$cliente->cpfCnpj?></span>
                                                            </p>
                                                            <div class="text-muted text-center">Criado em: <?=date("d/m/Y H:i", strtotime($cliente->created_at));?></div>
                                                            
                                                            <?php if ($cliente->updated_at) { ?>
                                                                <div class="text-muted text-center">Última alteração: <?=date("d/m/Y H:i", strtotime($cliente->updated_at));?></div>
                                                            <?php } ?>

                                                            <div class="text-muted text-center">Status: <?php echo ($cliente->ativo == 1) ? '<span class="badge badge-success">Ativo</span>' : '<span class="badge badge-danger">Desabilitado</span>'; ?></div>
                                                        </div>
                                                        <div class="card-footer box-profile text-center">
                                                            <a href="/clientes/create?clienteId=<?=$cliente->id?>&e=1" class="btn btn-xs btn-info link">
                                                                <i class="fas fa-edit"></i> Editar Dados
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="card">
                                                        <div class="card-header p-2">
                                                            <ul class="nav nav-pills">
                                                                <li class="nav-item"><a class="nav-link active" href="#enderecos" data-toggle="tab"><i class="fas fa-home"></i> Endereços do Cliente</a></li>
                                                                <li class="nav-item"><a class="nav-link" href="#contatos" data-toggle="tab"><i class="fas fa-phone"></i> Contatos do Cliente</a></li>
                                                                <li class="nav-item"><a class="nav-link" href="#acesso" data-toggle="tab"><i class="fas fa-key"></i> Acesso do Cliente</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="overlay-wrapper">     
                                                                <?php include "../app/views/partials/loading.php"; ?>
                                                                <div class="tab-content">
                                                                    <div class="active tab-pane" id="enderecos">
                                                                        <div class="card">
                                                                            <div class="card-header">
                                                                                <h3 class="card-title"><i class="fas fa-home"></i> Endereços do Cliente</h3>
                                                                            </div>

                                                                            <?php
                                                                                if ($cliente->tipoEndereco == 1) {
                                                                                    $iconend = "building-o";
                                                                                    $descend = "COMERCIAL";
                                                                                } else {
                                                                                    $iconend = "home";
                                                                                    $descend = "RESIDENCIAL";
                                                                                }
                                                                            ?>                                                                            
                                                                            <div class="card-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <strong><i class="fas fa-<?=$iconend?>"></i> <?=$descend?></strong>
                                                                                        <hr >
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <strong>CEP</strong>
                                                                                        <p class="text-muted" id="cep"><?=$cliente->cep?></p>

                                                                                        <strong>ENDEREÇO</strong>
                                                                                        <p class="text-muted"><?=$cliente->endereco?>, Nº <?=$cliente->numero?>. <?=$cliente->complemento?></p>

                                                                                        <strong>BAIRRO CIDADE/UF</strong>
                                                                                        <p class="text-muted"><?=$cliente->bairro?>, <?=$cliente->cidade?>/<?=$cliente->uf?></p>
                                                                                    </div>
                                                                                </div>                                                                                
                                                                                <hr>
                                                                            </div>
                                                                            <div class="card-footer text-right">
                                                                                <a href="/clientes/create?clienteId=<?=$cliente->id?>&e=2" class="btn btn-xs btn-info link">
                                                                                    <i class="fas fa-edit"></i> Editar Endereços
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="tab-pane" id="contatos">
                                                                        <div class="card">
                                                                            <div class="card-header">
                                                                                <h3 class="card-title"><i class="fas fa-phone"></i> Contatos do Cliente</h3>
                                                                            </div>

                                                                            <?php
                                                                                if ($cliente->tipoContato == 1) {
                                                                                    $buttonWhats = "<i class='fas fa-mobile-alt'></i> <span id='celular'>". $cliente->celular . '</span>';
                                                                                } else {
                                                                                    $buttonWhats = "<a href='' target='_blank' class='btn btn-success'><i class='fab fa-whatsapp'></i> <span id='celular'>". $cliente->celular ."</span></a>";
                                                                                }
                                                                            ?>
                                                                            
                                                                            <div class="card-body">
                                                                                <strong>EMAIL</strong>
                                                                                <p class="text-muted"><i class="fas fa-envelope"></i> <?=$cliente->email?></p>
                                                                                <hr>
                                                                                <?php if ($cliente->telefone) { ?>
                                                                                <strong>TELEFONE</strong>
                                                                                <p class="text-muted"><i class="fas fa-phone"></i> <span id="telefone"><?=$cliente->telefone?></span></p>
                                                                                <hr>
                                                                                <?php } ?>
                                                                                <strong>CELULAR</strong>
                                                                                <p class="text-muted">
                                                                                    <?=$buttonWhats?>
                                                                                </p>
                                                                                <hr>
                                                                            </div>
                                                                            <div class="card-footer text-right">
                                                                                <a href="/clientes/create?clienteId=<?=$cliente->id?>&e=3" class="btn btn-xs btn-info link">
                                                                                    <i class="fas fa-edit"></i> Editar Contatos
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="tab-pane" id="acesso">
                                                                        <div class="card">
                                                                            <div class="card-header">
                                                                                <h3 class="card-title"><i class="fas fa-key"></i> Acesso do Cliente</h3>
                                                                            </div>
                                                                            
                                                                            <div class="card-body">
                                                                                <strong>LOGIN</strong>
                                                                                <p class="text-muted"><i class="fas fa-envelope"></i> <?=$cliente->email?></p>
                                                                                <hr>
                                                                                <strong>SENHA</strong>
                                                                                <p class="text-muted"><i class="fas fa-eye-slash"></i> ************</p>
                                                                                <hr>
                                                                            </div>

                                                                            <div class="card-footer text-right">
                                                                                <a href="/clientes/create?clienteId=<?=$cliente->id?>&e=4" class="btn btn-xs btn-info link">
                                                                                    <i class="fas fa-edit"></i> Editar Acesso
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <?php include "../app/views/partials/copyright.php"; ?>
    </div>

    <?php include "../app/views/partials/modals.php"; ?>

<?php require '../app/views/partials/footer.php'; ?>
</body>
</html>