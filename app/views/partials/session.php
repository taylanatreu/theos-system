<?php 

$ctrlSessao = isset($_SESSION["ctrlSessao"]) ? unserialize($_SESSION["ctrlSessao"]) : NULL;

if ($ctrlSessao && !empty($ctrlSessao)) {
    
    $ctrUsuario     = $ctrlSessao["ctrUsuario"];
    $ctrLogin       = $ctrlSessao["ctrLogin"];
    $ctrDash        = $ctrlSessao["ctrDash"];
    $ctrErrors      = $ctrlSessao["ctrErrors"];    
    $ctrGpu         = $ctrlSessao["ctrGpu"];
    $ctrGper        = $ctrlSessao["ctrGper"];
    $ctrGrupo       = $ctrlSessao["ctrGrupo"];
    $ctrLog         = $ctrlSessao["ctrLog"];
    $ctrCliente     = $ctrlSessao["ctrCliente"];
    
    $currentRoute = $_SERVER['REQUEST_URI'];

    $userId = isset($_SESSION["user_id"]) ? $_SESSION["user_id"] : "";
    $userToken = isset($_COOKIE['remember_token']) ? $_COOKIE['remember_token'] : null;

    $verificarToken = $ctrUsuario->verificarCookie($userId, $userToken);
    $pegaUsuario = $ctrUsuario->recuperaUsuario($userId);

    if ($verificarToken) {
        if ($currentRoute == "/login") {
            header('Location: /dashboard');
            exit();    
        }

    } else {
        if (!$userId && $currentRoute != "/login") {
            header('Location: /login');
            exit();    
        }
        
    }

} else {
    if ($currentRoute != "/login") {
        header('Location: /login');
        exit();
    }
}
?>