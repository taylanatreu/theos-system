<?php
    $pageScripts = array(
        'welcomeModal' => array('/dashboard'),
        'formAuth' => array('/login'),
        'formCadastro' => array('/grupos', '/grupos-permissoes', '/grupos-usuarios', '/historico-logs', '/usuarios'),
        'formCliente' => array('/clientes', '/clientes/create'),
        'formPermissions' => array('/grupos-permissoes', '/grupos-usuarios'),
        'formProfile' => array('/profile')
    );

    $pathPage = $_SERVER['REQUEST_URI'];
    $parsedUrl = parse_url($pathPage);
    $urlWithoutQuery = $parsedUrl['path'];
    $currentPage = $urlWithoutQuery;

    foreach ($pageScripts as $script => $pages) {
        if (in_array($currentPage, $pages)) {
            echo "<script src='../../../js/utils/{$script}.js'></script>";
        }
    }
?>