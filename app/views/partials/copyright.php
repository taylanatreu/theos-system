<footer class="main-footer">
  Copyright &copy; <?=date("Y")?> <strong>THEÓS</strong> Sistema de Cobrança Integrada.<br />
  Todos os direitos reservados
  <div class="float-right d-sm-inline-block">
    <b>Versão</b> 1.0
  </div>
</footer>