<?php include "../app/views/partials/load-page.php"; ?>

<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto">

        <li class="nav-item dropdown">
            <a class="nav-link" href="/dashboard">
                <i class="fas fa-home" style="font-size: 22px;"></i>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" id="btnNotificacao">
                <i class="far fa-bell" style="
                font-size: 22px;"></i>
                <span class="badge badge-danger navbar-badge" id="notificacao-nao-lido"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right"  style="min-width: 480px;">
                <span class="dropdown-header text-left">Notificações</span>
                <div class="dropdown-divider"></div>
                <div class="dropdown-item"><i class="fas fa-bell-slash"></i> Você não possui nenhuma notificação.</div>
                <div class="dropdown-divider"></div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt" style="font-size: 22px;"></i>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link notificacao" data-toggle="dropdown" href="#" aria-expanded="false">
                <?php
                if ($pegaUsuario->avatar) {
                ?>
                    <img 
                    src="/storage/users/<?=$pegaUsuario->avatar?>" 
                    class="img-circle" 
                    id="imgPerfil"
                    alt="<?=ucfirst($pegaUsuario->nome)?>"
                    style="width: 30px;">
                <?php } else { ?>
                    <i class="fas fa-user"></i>          
                <?php } ?>
                <strong><span id="infoPerfil"><?=ucfirst($pegaUsuario->nome)?></span></strong> <br/>  
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="/profile" class="dropdown-item link">
                    <i class="fas fa-circle-user mr-2"></i> Meu Perfil
                </a>
                <div class="dropdown-divider"></div>
                    <a href="/logout" class="dropdown-item link">
                    <i class="fas fa-power-off mr-2"></i> Sair
                </a>
            </div>
        </li>
    </ul>
</nav>