<?php
function isActive($routes)
{
    $currentPath = strtok($_SERVER['REQUEST_URI'], '?');
    if (!is_array($routes)) {
        $routes = [$routes];
    }
        foreach ($routes as $route) {
        if (substr($currentPath, 0, strlen($route)) === $route) {
            return 'active';
        }
    }
    return '';
}

function isMenuActive($routes)
{
    $currentPath = strtok($_SERVER['REQUEST_URI'], '?');
    if (!is_array($routes)) {
        $routes = [$routes];
    }
        foreach ($routes as $route) {
        if (substr($currentPath, 0, strlen($route)) === $route) {
            return 'menu-open';
        }
    }
    return '';
}
?>
<aside class="main-sidebar sidebar-dark-orange elevation-4">
    <a href="/dashboard" class="brand-link" style="text-align:center;">
      <img src="../../../img/logo-branco.png" alt="THEOS" width="150">
    </a>
    
    <div class="sidebar">

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          
          <li class="nav-item">
            <a href="/dashboard" class="nav-link <?= isActive(['/dashboard']); ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>DASHBOARD</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/clientes" class="nav-link <?= isActive(['/clientes']); ?> link">
              <i class="nav-icon fas fa-users"></i>
              <p>CLIENTES</p>
            </a>
          </li>

          <li class="nav-header">SISTEMA</li>

          <li class="nav-item <?= isMenuActive(['/grupos']); ?> ">
              <a href="#" class="nav-link <?= isActive(['/grupos']); ?>">
                  <i class="nav-icon fas fa-lock"></i>
                  <p> SEGURANÇA <i class="right fas fa-angle-left"></i></p>
              </a>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                      <a href="/grupos" class="nav-link <?= isActive('/grupos'); ?>">
                          <i class="far fa-circle nav-icon"></i>
                          <p>GRUPOS & ACESSOS</p>
                      </a>
                  </li>
              </ul>
          </li>
          <li class="nav-item">
            <a href="/usuarios" class="nav-link <?= isActive(['/usuarios']); ?>">
              <i class="nav-icon fas fa-users-cog"></i>
              <p>USUÁRIOS</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/historico-logs" class="nav-link <?= isActive(['/historico-logs']); ?>">
              <i class="nav-icon fas fa-search"></i>
              <p>HISTÓRICO DE LOGS</p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>