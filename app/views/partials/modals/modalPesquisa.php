<form id="formPesquisaAvancada">
    <div class="modal fade" id="filtroPesquisaModal" tabindex="-1" role="dialog" aria-labelledby="filtroPesquisaModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="filtroPesquisaModalLabel">
                        <strong><i class="fas fa-filter"></i> Filtro de Pesquisa</strong> <br />
                        <small>Selecione abaixo os filtros que deseja aplicar na pesquisa:</small>
                    </h5>
                    <button onclick="construirUrl('')" type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="camposPersonalizados" style="display: none;">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fTipoCliente"><small>Tipo de Pessoa:</small></label>
                                <select id="fTipoCliente" name="fTipoCliente" class="select2" style="width: 100%;">
                                    <option selected value="">Todos</option>
                                    <option value="1">Física</option>
                                    <option value="2">Jurídica</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fTipoEndereco"><small>Tipo de Endereço:</small></label>
                                <select id="fTipoEndereco" name="fTipoEndereco" class="select2" style="width: 100%;">
                                    <option selected value="">Todos</option>
                                    <option value="1">Comercial</option>
                                    <option value="2">Residencial</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="periodoBusca"><small>Período de criação:</small></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="fPeriodo" id="periodoBusca" class="form-control float-right" placeholder="Selecione o período">
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fStatus"><small>Status:</small></label>
                                <select id="fStatus" name="fStatus" class="select2" style="width: 100%;">
                                    <option selected value="">Todos</option>
                                    <option value="1">Ativos</option>
                                    <option value="0">Desabilitados</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fOrder"><small>Ordenar por:</small></label>
                                <select name="fOrder" id="fOrder" class="select2" style="width: 100%;" required>
                                    <option value="descricao" selected>Descrição/Nome</option>
                                    <option value="created_at">Data de Cadastro</option>
                                    <option value="created_at">Última Alteração</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="termoBuscaAvancada"><small>Digite o que deseja procurar:</small></label>
                                <div class="input-group input-group-lg">
                                    <input 
                                        type="search" 
                                        id="termoBuscaAvancada"
                                        class="form-control form-control-lg" 
                                        placeholder="Digite as palavras chaves da sua busca aqui">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-lg btn-primary">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button 
                        type="button" 
                        onclick="construirUrl('')" 
                        data-dismiss="modal" 
                        class="btn btn-default">
                        <i class="fas fa-eraser"></i> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>