<div 
    class="modal fade" 
    id="confirmacaoRemoverModal" 
    tabindex="-1" role="dialog" 
    aria-labelledby="confirmacaoRemoverModalLabel" 
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmacaoRemoverModalLabel">Confirmação de Exclusão da Imagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <i class="fas fa-exclamation-triangle"></i> Tem certeza que deseja excluir a imagem?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" id="confirmarRemoverBtn"><i class="fas fa-trash"></i> Confirmar</button>
            </div>
        </div>
    </div>
</div>