<div 
    class="modal modal fade" 
    id="BoasVindasModal" 
    tabindex="-1" role="dialog" 
    aria-labelledby="BoasVindasModalLabel" 
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="BoasVindasModalLabel"><strong>Seja Bem-Vindo!</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">            
                <div class="row">
                    <div class="col-md-6">
                        <h5>Nós somos a Theós, e queremos que você tenha uma nova experiência em nossa plataforma, 
                        e na forma de fazer sua gestão em cobranças, por isso em breve estará disponível 
                        novos módulos de bancos para sua empresa usar, além de novas funcionalidades.</h5>
                    </div>
                    <div class="col-md-6">
                        <img src="../img/banner-home.png" class="img-fluid" />
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="justify-content: flex-start;">
                <div class="form-group clearfix">                                                
                    <div class="icheck-success d-inline">
                        <input id="ocultarMsg" type="checkbox">
                        <label for="ocultarMsg"></label>
                    </div>
                    <label for="ocultarMsg" id="label-msg">Não exibir mais essa mensagem de boas-vindas!</label>
                </div>
            </div>
        </div>
    </div>
</div>