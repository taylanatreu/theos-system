<div 
class="modal fade" 
id="confirmacaoDesabilitaModal" 
tabindex="-1" 
role="dialog" 
aria-labelledby="confirmacaoDesabilitarModalLabel" 
aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmacaoDesabilitarModalLabel">Confirmação de Desabilitação de Registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <i class="fas fa-exclamation-triangle"></i> Tem certeza que deseja desabilitar este registro?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-warning" id="confirmarDesabilitarBtn"><i class="fas fa-ban"></i> Confirmar</button>
            </div>
        </div>
    </div>
</div>