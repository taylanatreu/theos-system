<div 
    class="modal fade" 
    id="confirmacaoExclusaoModal" 
    tabindex="-1" role="dialog" 
    aria-labelledby="confirmacaoExclusaoModalLabel" 
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmacaoExclusaoModalLabel">Confirmação de Exclusão de Registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <i class="fas fa-exclamation-triangle"></i> Tem certeza que deseja excluir este registro?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" id="confirmarExclusaoBtn"><i class="fas fa-trash"></i> Confirmar</button>
            </div>
        </div>
    </div>
</div>