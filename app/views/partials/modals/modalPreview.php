<div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="previewModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="previewModalLabel">Cortar e Salvar Imagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img id="previewImage" src="#" alt="Preview Image">

                <div><small>Tamanho máximo permitido é de 2MB.</small></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="closePreview" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-eraser"></i> Limpar
                </button>
                <button type="button" class="btn btn-primary" id="cropAndSave"><i class="fas fa-cloud-arrow-up"></i> Confirmar</button>
            </div>
        </div>
    </div>
</div>