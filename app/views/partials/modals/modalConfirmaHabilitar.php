<div 
class="modal fade" 
id="confirmacaoHabilitaModal" 
tabindex="-1" role="dialog" 
aria-labelledby="confirmacaoHabilitaModalLabel" 
aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmacaoHabilitaModalLabel">Confirmação de Habilitação de Registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <i class="fas fa-exclamation-triangle"></i> Tem certeza que deseja habilitar este registro?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" id="confirmarHabilitarBtn"><i class="fas fa-check"></i> Confirmar</button>
            </div>
        </div>
    </div>
</div>