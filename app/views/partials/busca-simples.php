<?php
$termoBusca = isset($_GET['termo_busca']) ? $_GET['termo_busca'] : '';
?>
<form id="formPesquisa">
    <?php if (isset($grupoId)) { ?>
        <input name="grupoId" id="grupoId" type="hidden" value="<?= $grupoId ?>">
    <?php } ?>

    <div class="input-group input-group-sm" style="width: 300px;">
        <input type="search" name="termo_busca" id="termoBuscaSimples" class="form-control float-right" value="<?=$termoBusca?>" placeholder="Pesquisar...">

        <div class="input-group-append">
            <button type="submit" class="btn btn-primary">
                <i class="fas fa-search"></i>
            </button>
        </div>
    </div>
</form>
