<?php 
    if (isset($grupoId)) {
        $pGrupo = "&grupoId=" . $grupoId; 
    } else { 
        $pGrupo = "";  
    }
?>

<div class="float-left" id="pagination">  
    Exibindo <?=$registrosPorPagina?> registros da página <?=$paginaAtual?>/<?=$totalPaginas?> de um total de <?=$totalRegistros?> registros.
</div>

<ul class="pagination pagination-sm m-0 float-right">

    <?php if ($totalPaginas == 1) { ?>
        <li class="page-item disabled">
            <a class="page-link link" href="#">«</a>
        </li>
        <li class="page-item disabled">
            <a class="page-link link" href="#">1</a>
        </li>

        <li class="page-item disabled">
            <a class="page-link link" href="#">»</a>
        </li>
    <?php } ?>

    <?php if ($totalPaginas > 1) { ?>
        <li class="page-item <?= $paginaAtual == 1 ? 'disabled' : '' ?>">
            <a class="page-link link" href="?page=<?= $paginaAtual - 1 ?><?=$pGrupo?>">«</a>
        </li>

        <?php for ($i = 1; $i <= $totalPaginas; $i++) : ?>
            <li class="page-item <?= $paginaAtual == $i ? 'active' : '' ?>">
                <a class="page-link link" href="?page=<?= $i ?><?=$pGrupo?>"><?= $i ?></a>
            </li>
        <?php endfor; ?>

        <li class="page-item <?= $paginaAtual == $totalPaginas ? 'disabled' : '' ?>">
            <a class="page-link link" href="?page=<?= $paginaAtual + 1 ?>">»</a>
        </li>
    <?php } ?>    
    
</ul>