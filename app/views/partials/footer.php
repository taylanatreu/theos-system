<script src="../../../assets/plugins/jquery/jquery.min.js"></script>
<script src="../../../assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../../assets/plugins/inputmask/jquery.inputmask.js"></script>
<script src="../../../assets/plugins/select2/js/select2.full.min.js"></script>
<script src="../../../assets/dist/js/adminlte.min.js"></script>
<script src="../../../assets/dist/js/moment.min.js"></script>
<script src="../../../assets/plugins/toastr/toastr.min.js"></script>
<script src="../../../assets/plugins/bs-stepper/js/bs-stepper.min.js"></script>

<?php include "../app/views/partials/global-utils.php"; ?>

<?php include "../app/views/partials/global-forms.php"; ?>
