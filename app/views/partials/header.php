<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>THEÓS Sistema de Cobrança Integrada</title>

  <link rel="icon" href="../img/favicon.png" type="image/x-icon">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../../assets/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="../../../assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="../../../assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <link rel="stylesheet" href="../../../assets/plugins/select2/css/select2.min.css">
  
  <link rel="stylesheet" href="../../../assets/dist/css/adminlte.css">
  <link rel="stylesheet" href="../../../assets/plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="../../../assets/dist/css/cropper.css">

  <link rel="stylesheet" href="../../../assets/plugins/bs-stepper/css/bs-stepper.min.css">

  <link rel="stylesheet" href="../../../assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">

<link rel="stylesheet" href="../../../assets/plugins/daterangepicker/daterangepicker.css">
</head>