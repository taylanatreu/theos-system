<?php 
require '../app/views/partials/header.php';
require '../app/views/partials/session.php';
?>

<body class="hold-transition login-page">
  <div class="container-fluid">
    
    <div class="row mb-2">
        <div class="col-md-1 text-right"></div>
        <div class="col-md-4 d-none d-md-block" style="text-align: right;">
          <img src="/img/homepage.png" />
        </div>
        <div class="col-md-4">
          <div class="login-box">  
            <div class="login-card-body">
              <a href="/login">
                <img src="img/logo.png" class="img-fluid">
              </a>

              <form id="formAuth">
                <div class="row mb-3">
                  <div class="col-md-12">
                    <div class="form-group">
                      <div class="input-group mb-3">
                        <input name="login" type="email" autocomplete="on" class="form-control form-control-border" id="email" placeholder="Digite seu Email" required>
                        <div class="input-group-append">
                          <span class="fas fa-user" id="emailLogin"></span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="input-group">                
                        <input name="senha" type="password" class="form-control form-control-border" id="senha" placeholder="Digite sua Senha" required>
                        <div class="input-group-append">
                          <a class="" id="viewPass">
                            <i class="far fa-eye"></i>
                          </a>
                        </div>
                      </div>
                    </div>                    
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="icheck-primary d-inline">
                          <input name="remember" value="1" type="checkbox" id="remember">
                          <label for="remember">Lembar-me</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <a href="">Esqueci a senha</a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-key"></i> ENTRAR</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>

  <?php require '../app/views/partials/footer.php'; ?>
</body>
</html>