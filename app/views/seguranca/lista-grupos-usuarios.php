<?php 
require '../app/views/partials/header.php';
require '../app/views/partials/session.php';
?>
<body class="hold-transition sidebar-mini  layout-fixed">
    <div class="wrapper">
        <?php 
            include "../app/views/partials/navbar.php";
            include "../app/views/partials/sidebar-menu.php";
        ?>
        <!-- CONTEUDO DA PAGINA -->
        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0"><i class="fas fa-users"></i> Usuários do Grupo: <strong><?=$grupo->name?></strong></h1>
                            <small>Gerencie os usuários que fazem parte dos grupos de acesso.</small>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/dashboard" class="link">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="/grupos" class="link">Grupos de Acesso</a></li>
                            <li class="breadcrumb-item active">Usuários do Grupo: <strong><?=$grupo->name?></strong></li>
                            </ol>
                        </div>
                    </div>

                    <div class="text-right">
                        <a href="/grupos" class="btn btn-xs btn-default link"><i class="fas fa-shield-alt"></i> Lista de Grupos</a>
                        <a href="/usuarios" class="btn btn-xs btn-success link"><i class="fas fa-user"></i> Novo Usuário</a>
                    </div>
                </div>
            </div>
            
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- LISTA DE GRUPOS -->
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fa fa-search"></i> Lista de Usuários do Grupo: <strong><?=$grupo->name?></strong></h3>

                                    <div class="card-tools">
                                        <?php include "../app/views/partials/busca-simples.php" ?>
                                    </div>
                                </div>
                                <div class="card-body table-responsive p-0">
                                    <div class="overlay-wrapper">
                                        <?php include "../app/views/partials/loading.php"; ?>
                                        <table class="table table-hover table-striped text-nowrap">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ID</th>
                                                    <th class="text-center">USUÁRIO</th>
                                                    <th class="text-center">EMAIL</th>
                                                    <th class="text-center">INCLUIR/REMOVER</th>
                                                </tr>
                                            </thead>
                                            <tbody id="registros">
                                                <?php 
                                                $x = 1;
                                                if (isset($usuariosAtivos) && is_array($usuariosAtivos) && !empty($usuariosAtivos)) {
                                                foreach($usuariosAtivos as $usuario) { 
                                                    if ($this->verificacao($usuario->id, $grupoId)) {
                                                        $checado = "checked";
                                                    } else {
                                                        $checado = "";
                                                    }
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?=$x?></td>
                                                    <td class="text-center"><?=$usuario->nome?> <?=$usuario->sobrenome?></td>                                            
                                                    <td class="text-center"><?=$usuario->email?></td>
                                                    <td class="text-center">
                                                        <div class="form-group">
                                                            <div class="custom-control custom-switch custom-switch-off-default custom-switch-on-success">
                                                                <input 
                                                                    type="checkbox" 
                                                                    class="custom-control-input usuario-switch" 
                                                                    id="usuario-<?=$usuario->id?>"
                                                                    data-user-id="<?=$usuario->id?>" 
                                                                    data-grupo-id="<?=$grupoId?>"
                                                                    <?=$checado?>>
                                                                <label 
                                                                    class="custom-control-label" 
                                                                    for="usuario-<?=$usuario->id?>"></label>                                                                
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php $x ++; } } else { ?>
                                                    <tr>
                                                        <td class="text-center" colspan="4">Nenhum registro encontrado.</td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <?php include "../app/views/partials/paginacao.php"; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <?php include "../app/views/partials/copyright.php"; ?>
    </div>

    <?php include "../app/views/partials/modals.php"; ?>

<?php require '../app/views/partials/footer.php'; ?>
</body>
</html>