<?php 
require '../app/views/partials/header.php';
require '../app/views/partials/session.php';
?>
<body class="hold-transition sidebar-mini  layout-fixed">
    <div class="wrapper">
    <?php 
    include "../app/views/partials/navbar.php";
    include "../app/views/partials/sidebar-menu.php";
    ?>

    <!-- CONTEUDO DA PAGINA -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"><i class="fas fa-shield-alt"></i> Grupos de Acesso</h1>
                        <small>Gerencie os grupos de perfil de usuários do sistema.</small>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard" class="link">Dashboard</a></li>
                        <li class="breadcrumb-item active">Grupos de Acesso</li>
                        </ol>
                    </div>
                </div>

                <div class="text-right">
                    <button type="button" class="btn btn-success btn-xs" id="novoCadastro" title="Novo Grupo"><i class="fas fa-plus-circle"></i> Novo Grupo</button>
                </div>
            </div>
        </div>
        
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <!-- CADASTRO DE GRUPOS -->
                        <div id="cardCadastro" style="display: none;">
                            <?php include "../app/views/seguranca/forms/form-cadastro-grupo.php"; ?>
                        </div>
                        
                        <!-- LISTA DE GRUPOS -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title"><i class="fa fa-search"></i> Lista de Grupos de Acesso</h3>

                                <div class="card-tools">
                                    <?php include "../app/views/partials/busca-simples.php" ?>
                                </div>
                            </div>
                            <div class="card-body table-responsive p-0">
                                <div class="overlay-wrapper">
                                    <?php include "../app/views/partials/loading.php"; ?>
                                    <table class="table table-hover table-striped text-nowrap">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">NOME</th>
                                                <th class="text-center">DESCRIÇÃO</th>
                                                <th class="text-center">STATUS</th>
                                                <th class="text-center">AÇÕES</th>
                                                <th class="text-center"><i class="fas fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody id="registros">
                                            <?php 
                                            $x = 1;
                                            if (isset($grupos) && is_array($grupos) && !empty($grupos)) {
                                            foreach($grupos as $grupo) { 
                                                ?>
                                            <tr id="registro<?=$grupo->id?>">
                                                <td class="text-center"><?=$x?></td>
                                                <td class="text-center"><?=$grupo->name?></td>
                                                <td class="text-center"><?=$grupo->descricao?></td>
                                                <td class="text-center">
                                                    <?php if ($grupo->ativo == 1) { ?>
                                                        <i class="far fa-check-square"></i>
                                                    <?php } else { ?>
                                                        <i class="fas fa-ban"></i>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center">
                                                    <button 
                                                        type="button" 
                                                        class="btn btn-xs btn-info" 
                                                        title="Editar Grupo" 
                                                        onclick="editarRegistro(<?=$grupo->id?>)"
                                                        >
                                                        <i class="fas fa-edit"></i>
                                                    </button>    
                                                    
                                                    <?php if ($grupo->ativo == 1) { ?>
                                                        <a 
                                                            href="#" 
                                                            class="btn btn-xs btn-warning" 
                                                            data-toggle="modal" 
                                                            data-target="#confirmacaoDesabilitaModal" 
                                                            data-id="<?=$grupo->id?>"
                                                            title="Desabilitar Grupo">
                                                            <i class="fas fa-ban"></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a 
                                                            href="#" 
                                                            class="btn btn-xs btn-success" 
                                                            data-toggle="modal" 
                                                            data-target="#confirmacaoHabilitaModal" 
                                                            data-id="<?=$grupo->id?>" 
                                                            title="Habilitar Grupo">
                                                            <i class="fas fa-check"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <a 
                                                        href="#" 
                                                        class="btn btn-xs btn-danger" 
                                                        title="Remover Grupo" 
                                                        data-toggle="modal" 
                                                        data-target="#confirmacaoExclusaoModal" 
                                                        data-id="<?=$grupo->id?>">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <a 
                                                        href="/grupos-usuarios?grupoId=<?=$grupo->id?>" 
                                                        class="btn btn-xs btn-dark link">
                                                        <i class="fas fa-users"></i> Usuários
                                                    </a>

                                                    <a 
                                                        href="/grupos-permissoes?grupoId=<?=$grupo->id?>" 
                                                        class="btn btn-xs btn-success link" 
                                                        title="Visualizar Permissões">
                                                        <i class="fas fa-users"></i> Permissões
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php $x ++; } } else { ?>
                                                <tr>
                                                    <td class="text-center" colspan="6">Nenhum registro encontrado.</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?php include "../app/views/partials/paginacao.php"; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php include "../app/views/partials/copyright.php"; ?>
    </div>

    <?php include "../app/views/partials/modals.php"; ?>

<?php require '../app/views/partials/footer.php'; ?>
</body>
</html>