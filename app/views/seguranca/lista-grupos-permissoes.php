<?php 
require '../app/views/partials/header.php';
require '../app/views/partials/session.php';
?>
<body class="hold-transition sidebar-mini  layout-fixed">
    <div class="wrapper">
    <?php 
    include "../app/views/partials/navbar.php";
    include "../app/views/partials/sidebar-menu.php";
    ?>

    <!-- CONTEUDO DA PAGINA -->
    <div class="content-wrapper">
        <div class="content-header">
        <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"><i class="fas fa-users"></i> Permissões do Grupo: <strong><?=$grupo->name?></strong></h1>
                        <small>Gerencie as permissões que cada grupo terá acesso.</small>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/grupos">Grupos de Acesso</a></li>
                        <li class="breadcrumb-item active">Permissões do Grupo: <strong><?=$grupo->name?></strong></li>
                        </ol>
                    </div>
                </div>
                <div class="text-right">
                    <a href="/grupos" class="btn btn-xs btn-default link" title="Lista de Grupos"><i class="fas fa-shield-alt"></i> Lista de Grupos</a>
                    <button type="button" class="btn btn-xs btn-success" id="novoCadastro" title="Nova Permissão"><i class="fas fa-plus-circle"></i> Nova Permissão</button>
                </div>
            </div>
        </div>
        
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <!-- CADASTRO DE PERMISSÕES -->
                        <div id="cardCadastro" style="display: none;">
                            <?php include "../app/views/seguranca/forms/form-cadastro-permissao.php"; ?>
                        </div>
                        
                        <!-- LISTA DE GRUPOS -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title"><i class="fa fa-search"></i> Lista de Permissões do Grupo: <strong><?=$grupo->name?></strong></h3>

                                <div class="card-tools">
                                    <?php include "../app/views/partials/busca-simples.php" ?>
                                </div>
                            </div>
                            <div class="card-body table-responsive p-0">
                                <div class="overlay-wrapper">
                                    <?php include "../app/views/partials/loading.php"; ?>
                                    <table class="table table-hover table-striped text-nowrap">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">PERMISSÃO</th>
                                                <th class="text-center">STATUS</th>
                                                <th class="text-center">AÇÕES</th>
                                                <th class="text-center">INCLUIR/REMOVER</th>
                                            </tr>
                                        </thead>
                                        <tbody id="registros">
                                            <?php 
                                            $x = 1;
                                            if (isset($permissoes) && is_array($permissoes) && !empty($permissoes)) {
                                            foreach($permissoes as $permissao) { 
                                                ?>
                                            <tr>
                                                <td class="text-center" style="width: 10%;"><?=$x?></td>
                                                <td class="text-center"><?=$permissao->descricao?></td>
                                                <td class="text-center">
                                                    <?php if ($permissao->ativo == 1) { ?>
                                                        <i class="far fa-check-square"></i>
                                                    <?php } else { ?>
                                                        <i class="fas fa-ban"></i>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center">
                                                    <button 
                                                        type="button" 
                                                        class="btn btn-xs btn-info" 
                                                        title="Editar Permissão" 
                                                        onclick="editarRegistro(<?=$permissao->id?>)">
                                                        <i class="fas fa-edit"></i>
                                                    </button>    
                                                    
                                                    <?php if ($permissao->ativo == 1) { ?>
                                                        <a 
                                                            href="#" 
                                                            class="btn btn-xs btn-warning" 
                                                            data-toggle="modal" 
                                                            data-target="#confirmacaoDesabilitaModal" 
                                                            data-id="<?=$permissao->id?>"
                                                            data-grupo="<?=$grupoId?>"
                                                            title="Desabilitar Permissão">
                                                            <i class="fas fa-ban"></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a 
                                                            href="#" 
                                                            class="btn btn-xs btn-success" 
                                                            data-toggle="modal" 
                                                            data-target="#confirmacaoHabilitaModal" 
                                                            data-id="<?=$permissao->id?>"
                                                            data-grupo="<?=$grupoId?>" 
                                                            title="Habilitar Permissão">
                                                            <i class="fas fa-check"></i>
                                                        </a>
                                                    <?php } ?>

                                                    <a 
                                                        href="#" 
                                                        class="btn btn-xs btn-danger" 
                                                        title="Remover Permissão" 
                                                        data-toggle="modal" 
                                                        data-target="#confirmacaoExclusaoModal" 
                                                        data-id="<?=$permissao->id?>"
                                                        data-grupo="<?=$grupoId?>"
                                                        >
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </td>
                                                <td class="text-center" style="width: 20%;">
                                                    <div class="form-group">
                                                        <div class="custom-control custom-switch custom-switch-off-default custom-switch-on-success">
                                                            <input 
                                                                type="checkbox" 
                                                                class="custom-control-input permissao-switch" 
                                                                id="permissao-<?=$permissao->id?>"
                                                                data-permissao-id="<?=$permissao->id?>" 
                                                                data-grupo-id="<?=$grupoId?>"
                                                                <?= $this->verificacao($permissao->id, $grupoId) ? 'checked' : '' ?>>
                                                            <label class="custom-control-label" for="permissao-<?=$permissao->id?>"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $x ++; } } else { ?>
                                                <tr>
                                                    <td class="text-center" colspan="5">Nenhum registro encontrado.</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?php include "../app/views/partials/paginacao.php"; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php include "../app/views/partials/copyright.php"; ?>
    </div>

    <?php include "../app/views/partials/modals.php"; ?>

<?php require '../app/views/partials/footer.php'; ?>
</body>
</html>