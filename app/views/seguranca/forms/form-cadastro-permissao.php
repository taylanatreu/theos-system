<div class="card">
    <div class="card-header">
        <h3 class="card-title"><i class="fas fa-plus-circle"></i> Cadastro de Permissões</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" id="closeBtn">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <form id="formCadastro">
        
        <input name="registroId" id="registroId" type="hidden" value="">
        <input name="grupoId" id="grupoId" type="hidden" value="<?=$grupo->id?>">

            <div class="card-body">
                <div class="row">
                    <div class="col-md-11">
                        <div class="form-group">
                            <label for="descricao">Descrição</label>
                            <input name="descricao" type="text" class="form-control" id="descricao" required>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group clearfix">                                                
                            <label for="checkboxPrimary1">Ativo</label><br />
                            <div class="icheck-success d-inline">
                                <input name="ativo" value="1" type="checkbox" id="checkboxPrimary1" checked="">
                                <label for="checkboxPrimary1">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="card-footer">
            <div class="form-group text-right">
                <button type="button" class="btn btn-default" onclick="limparFormCadastro('formCadastro')"><i class="fas fa-eraser"></i> Limpar</button>
                <button type="submit" class="btn btn-primary" id="btnSalvar"><i class="fas fa-save"></i> Salvar</button>
            </div>
        </div>
    </form>
</div>