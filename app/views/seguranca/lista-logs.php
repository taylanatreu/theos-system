<?php 
require '../app/views/partials/header.php';
require '../app/views/partials/session.php';
?>
<body class="hold-transition sidebar-mini  layout-fixed">
    <div class="wrapper">
    <?php 
    include "../app/views/partials/navbar.php";
    include "../app/views/partials/sidebar-menu.php";
    ?>

    <!-- CONTEUDO DA PAGINA -->
    <div class="content-wrapper">
        <div class="content-header">
        <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"><i class="fas fa-search"></i> Histórico de Logs</h1>
                        <small>Visualize todos os logs de ações que seu usuário realizou no sistema.</small>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard" class="link">Dashboard</a></li>
                        <li class="breadcrumb-item active">Histórico de Logs</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <!-- LISTA DE GRUPOS -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title"><i class="fa fa-search"></i> Lista de Logs do Usuário</h3>

                                <div class="card-tools">
                                    <?php include "../app/views/partials/busca-simples.php" ?>
                                </div>
                            </div>
                            <div class="card-body table-responsive p-0">
                                <div class="overlay-wrapper">
                                    <?php include "../app/views/partials/loading.php"; ?>
                                    <table class="table table-hover table-striped text-nowrap">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">USUÁRIO</th>
                                                <th class="text-center">AÇÃO</th>
                                                <th class="text-center">STATUS</th>
                                                <th class="text-center"><i class="fas fa-calendar-alt"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody id="registros">
                                            <?php 
                                            $x = 1;
                                            if (isset($logs) && is_array($logs) && !empty($logs)) {
                                            foreach($logs as $log) { 
                                                ?>
                                            <tr>
                                                <td class="text-center" style="width: 10%;"><?=$x?></td>
                                                <td class="text-center"><?=$log->nome?> <?=$log->sobrenome?></td>
                                                <td class="text-center">
                                                    <?=$log->descricao?>
                                                </td>
                                                <td class="text-center">
                                                    <?php if ($log->status == 'error') { $bstatus = "danger"; } else { $bstatus = "success"; } ?>
                                                    <div class="badge badge-<?=$bstatus?>"><?=$log->status?></div>
                                                </td>
                                                <td class="text-center">
                                                    <?=date("d/m/Y H:i", strtotime($log->created_at));?>
                                                </td>
                                            </tr>
                                            <?php $x ++; } } else { ?>
                                                <tr>
                                                    <td class="text-center" colspan="6">Nenhum registro encontrado.</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?php include "../app/views/partials/paginacao.php"; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php include "../app/views/partials/copyright.php"; ?>
    </div>

    <?php include "../app/views/partials/modals.php"; ?>

<?php require '../app/views/partials/footer.php'; ?>
</body>
</html>