<?php

namespace App\Models;

use Databases\Database;
use PDO;

class GrupoPermissaoModel
{
    private $database;
    private $globalsModel;

    public function __construct(Database $database, GlobalsModel $globalsModel)
    {
        $this->database = $database;
        $this->table = "grupos_permissoes";
        $this->globalsModel = $globalsModel;
    }


    public function inserirGrupoPermissao($dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $colunas = implode(', ', array_keys($dados));
        $valores = implode(', ', array_fill(0, count($dados), '?'));

        $query = "INSERT INTO $this->table ($colunas) VALUES ($valores)";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            die("Erro na execução da consulta: " . $stmt->errorInfo()[2]);
        }

        return true;
    }

    public function removerGrupoPermissao($dados)
    {
        $pdo = $this->database->getPDO();
        $sql = "DELETE FROM $this->table WHERE permissionId = ? AND grupoId = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $dados["permissionId"], PDO::PARAM_INT);
        $stmt->bindParam(2, $dados["grupoId"], PDO::PARAM_INT);
        $stmt->execute();

        if (!$stmt->execute()) {
            die("Erro na execução do processamento: " . $stmt->errorInfo()[2]);
        }

        return true;
    }

    public function verificaPermissaoGrupo($permissionId, $grupoId) {
        $query = "SELECT COUNT(*) FROM $this->table WHERE permissionId = ? AND grupoId = ?";
        $stmt = $this->database->getPDO()->prepare($query);
        $stmt->execute([$permissionId, $grupoId]);
        $count = $stmt->fetchColumn();
        return $count > 0;
    }
}
