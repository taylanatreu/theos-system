<?php

namespace App\Models;

use Databases\Database;
use PDO;
use PDOException;

class ClienteModel
{
    private $database;
    private $globalsModel;

    public function __construct(Database $database, GlobalsModel $globalsModel)
    {
        $this->database = $database;
        $this->table = "clientes";
        $this->globalsModel = $globalsModel;

    }

    public function inserirCliente($dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $colunas = implode(', ', array_keys($dados));
        $valores = implode(', ', array_fill(0, count($dados), '?'));

        $query = "INSERT INTO $this->table ($colunas) VALUES ($valores)";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);

        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        $clienteId = $this->database->getPDO()->lastInsertId();
        $cliente = $this->recuperaCliente($clienteId);
    
        return ['success' => true, 'message' => 'Cliente inserido com sucesso.', 'title' => 'Sucesso', 'dados' => $cliente];
    }

    public function editarCliente($id, $dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $atualizacoes = array();
        foreach ($dados as $coluna => $valor) {
            $atualizacoes[] = "$coluna = ?";
        }

        $atualizacoesStr = implode(', ', $atualizacoes);

        $query = "UPDATE $this->table SET $atualizacoesStr WHERE id = ?";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        $valoresParametros[] = $id;

        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            return ['success' => false, 'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        $cliente = $this->recuperaCliente($id);
    
        if (!$cliente) {
            return ['success' => false, 'title' => 'Falha no Processamento', 'message' => 'Erro ao recuperar dados do usuário após a edição.'];
        }
    
        return ['success' => true, 'message' => 'Cliente editado com sucesso.', 'title' => 'Sucesso', 'dados' => $cliente];
    }

    public function recuperaPorCpfCnpj($valor)
    {
        $pdo = $this->database->getPDO();

        $sql = "SELECT * FROM $this->table WHERE cpfCnpj = :valor";

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":valor", $valor, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ);
    }


    public function recuperaPorEmail($id, $email)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table WHERE email = :email";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":email", $email, PDO::PARAM_STR);
        $stmt->execute();

        $cliente = $stmt->fetch(PDO::FETCH_OBJ);

        if ($id) {
            if ($cliente && $cliente->id != $id) {
                return true;
            }
        } else {
            if ($cliente) {
                return true;
            }
        }        

        return false;
    }

    public function excluirCliente($id)
    {
        $pdo = $this->database->getPDO();
        $sql = "UPDATE $this->table SET removed_at = NOW() WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        
        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        return ['success' => true, 'message' => 'Cliente removido com sucesso.', 'title' => 'Sucesso'];
    }

    public function habilitarDesabilitarCliente($id, $value)
    {
        $pdo = $this->database->getPDO();
        $sql = "UPDATE $this->table SET ativo = ? WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $value, PDO::PARAM_INT);
        $stmt->bindParam(2, $id, PDO::PARAM_INT);
        
        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        return ['success' => true, 'message' => 'Cliente editado com sucesso.', 'title' => 'Sucesso'];
    }

    public function contaTodosRegistrosCliente()
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT COUNT(*) FROM $this->table WHERE removed_at IS NULL";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    public function recuperarTodosPaginadoCliente($registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT c.*, usu.avatar as avatar, usu.nome as nome, usu.sobrenome as sobrenome
                FROM $this->table as c
                LEFT JOIN usuarios as usu
                ON usu.id = c.userId
                WHERE removed_at IS NULL
                ORDER BY usu.nome ASC
                LIMIT :registrosPorPagina OFFSET :offset";

        $stmt = $pdo->prepare($sql);

        $stmt->bindParam(":registrosPorPagina", $registrosPorPagina, PDO::PARAM_INT);
        $stmt->bindParam(":offset", $offset, PDO::PARAM_INT); 
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function recuperaDadosPesquisa($nomeCampo, $valorCampo)
    {        
        if ($nomeCampo == 'cpfCnpj') {
            $table = $this->table;
            $descCampo = "CPF ou CNPJ";
        } else {
            $table = "usuarios";
            $descCampo = "Email";
        }

        $pdo = $this->database->getPDO();
        $sql = "SELECT COUNT(*) 
                FROM $table 
                WHERE $nomeCampo = :valorCampo ";
        $stmt = $pdo->prepare($sql);

        if ($nomeCampo == 'cpfCnpj') {
            $valorCampo = $this->globalsModel->transformAcentos($valorCampo);
            $stmt->bindParam(":valorCampo", $valorCampo, PDO::PARAM_INT);

        } else {
            $stmt->bindParam(":valorCampo", $valorCampo, PDO::PARAM_STR);
        }        
        
        if (!$stmt->execute()) {
            return [
                'success' => false,
                'title' => 'Falha no Processamento', 
                'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]
            ];
        }

        $registros = $stmt->fetchColumn();

        if ($registros > 0) {
            return [
                'success' => false,
                'title' => "Falha na verificação!", 
                'message' => 'O <strong>' . strtoupper($descCampo) . '</strong> digitado já está sendo usado.'
            ];

        } else {
            return [
                'success' => true,
                'title' => "Sucesso!", 
                'message' => 'O <strong>'. strtoupper($descCampo) . '</strong> está disponível para uso.'
            ];
        }
    }

}
