<?php

namespace App\Models;

use Databases\Database;
use PDO;
use PDOException;

class UsuarioModel
{
    private $database;
    private $globalsModel;

    public function __construct(Database $database, GlobalsModel $globalsModel)
    {
        $this->database = $database;
        $this->table = "usuarios";
        $this->globalsModel = $globalsModel;

    }

    public function buscarRegistrosUsuario($termoBusca, $registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $palavrasChave = explode(' ', $termoBusca);
        $condicoes = [];

        foreach ($palavrasChave as $index => $palavra) {
            $palavraSemAcento = $this->globalsModel->removerAcentos($palavra);

            $condicoes[] = "(nome LIKE :busca{$index} OR nome LIKE :buscaSemAcento{$index} 
                            OR sobrenome LIKE :busca{$index} OR sobrenome LIKE :buscaSemAcento{$index} 
                            OR email LIKE :busca{$index} OR email LIKE :buscaSemAcento{$index})";
        }

        if (!empty($termoBusca)) {
            $condicaoBusca = " AND " . implode(" OR ", $condicoes) . "";
        } else {
            $condicaoBusca = "";
        }

        $sql = "SELECT * FROM $this->table
                WHERE 1=1 
                $condicaoBusca 
                ORDER BY nome ASC
                LIMIT :registrosPorPagina OFFSET :offset";

        $stmt = $pdo->prepare($sql);

        if ($stmt) {
            
            if (!empty($termoBusca)) {
                foreach ($palavrasChave as $index => $palavra) {
                    $palavraSemAcento = $this->globalsModel->removerAcentos($palavra);
                
                    $buscaParam = "%{$palavra}%";
                    $buscaSemAcentoParam = "%{$palavraSemAcento}%";
                
                    $stmt->bindParam(":busca{$index}", $buscaParam, PDO::PARAM_STR);
                    $stmt->bindParam(":buscaSemAcento{$index}", $buscaSemAcentoParam, PDO::PARAM_STR);
                }
            }

            $stmt->bindParam(':registrosPorPagina', $registrosPorPagina, PDO::PARAM_INT);
            $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
            $stmt->execute();

            $resultados = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $resultados;
        } else {
            $errorInfo = $pdo->errorInfo();
            return ['success' => false, 'message' => "Erro na execução do processamento: " . $stmt->errorInfo()[2], 'title' => 'Falha no Processamento'];
        }
    }

    public function inserirUsuario($dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $email = $dados['email'];

        if ($this->recuperaEmailUsuario($id = NULL, $email)) {
            return ['success' => false,'title' => 'Falha', 'message' => 'O email que você escolheu já está sendo usado por outro usuário.'];
        }

        $colunas = implode(', ', array_keys($dados));
        $valores = implode(', ', array_fill(0, count($dados), '?'));

        $query = "INSERT INTO $this->table ($colunas) VALUES ($valores)";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }

        $userId = $this->database->getPDO()->lastInsertId();
        $usuario = $this->recuperaUsuario($userId);
    
        return ['success' => true, 'message' => 'Usuário inserido com sucesso.', 'title' => 'Sucesso', 'dados' => $usuario];
    }

    public function editarUsuario($id, $dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $email = $dados['email'];

        if ($this->recuperaEmailUsuario($id, $email)) {
            return ['success' => false,'title' => 'Falha', 'message' => 'O email que você escolheu já está sendo usado por outro usuário.'];
        }

        $atualizacoes = array();
        foreach ($dados as $coluna => $valor) {
            $atualizacoes[] = "$coluna = ?";
        }

        $atualizacoesStr = implode(', ', $atualizacoes);

        $query = "UPDATE $this->table SET $atualizacoesStr WHERE id = ?";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        $valoresParametros[] = $id;

        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            return ['success' => false, 'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        $usuario = $this->recuperaUsuario($id);
    
        if (!$usuario) {
            return ['success' => false, 'title' => 'Falha no Processamento', 'message' => 'Erro ao recuperar dados do usuário após a edição.'];
        }
    
        return ['success' => true, 'message' => 'Usuário editado com sucesso.', 'title' => 'Sucesso', 'dados' => $usuario];
    }

    public function editarProfileUsuario($id, $dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $email = $dados['email'];

        if ($this->recuperaEmailUsuario($id, $email)) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'O email que você escolheu já está sendo usado por outro usuário.'];
        }

        $atualizacoes = array();
        foreach ($dados as $coluna => $valor) {
            $atualizacoes[] = "$coluna = ?";
        }

        $atualizacoesStr = implode(', ', $atualizacoes);

        $query = "UPDATE $this->table SET $atualizacoesStr WHERE id = ?";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        $valoresParametros[] = $id;

        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }

        $usuario = $this->recuperaUsuario($id);
    
        return ['success' => true, 'message' => 'Perfil editado com sucesso.', 'title' => 'Sucesso', 'dados' => $usuario];
    }

    public function recuperaUsuario($id)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    public function recuperaEmailUsuario($id, $email)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table WHERE email = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $email, PDO::PARAM_STR);
        $stmt->execute();

        $usuario = $stmt->fetch(PDO::FETCH_OBJ);

        if ($id) {
            if ($usuario && $usuario->id != $id) {
                return true;
            }
        } else {
            if ($usuario) {
                return true;
            }
        }        

        return false;
    }

    public function recuperaEmailUsuarioCliente($id, $email)
    {
        $pdo = $this->database->getPDO();

        $sql = "SELECT * FROM $this->table 
                WHERE email = ?";

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $email, PDO::PARAM_STR);
        $stmt->execute();

        $resultado = $stmt->fetch(PDO::FETCH_OBJ);

        if ($id) {
            if ($resultado && $resultado->id != $id) {
                $emailExistente = true;
            }
        } else {
            if ($resultado) {
                $emailExistente = true;
            } else {
                $emailExistente = false;
            }
        }  

        return ["emailExistente" => $emailExistente];
    }

    public function autenticacao($email, $senha, $remember)
    {
        $user = $this->recuperaUsuarioPorEmail($email);

        if (!$user) {
            return ['success' => false, 'message' => 'O Email fornecido não foi encontrado.', 'title' => 'Falha na Autenticação' ];
        } else {
            
            if (!password_verify(trim($senha), $user->senha)) {
                return ['success' => false, 'message' => 'A senha está incorreta.', 'title' => 'Falha na Autenticação' ];
            }

            if ($user->ativo == 0) {
                return ['success' => false, 'message' => 'Entre em contato com o administrador do sistema.', 'title' => 'Usuário Bloqueado' ];
            }

            if ($user->ativo == 1 && $remember == 1) {
                $token = gerarTokenUnico();
                $validade_token = time() + (30 * 24 * 60 * 60); 
                setcookie('remember_token', $token, $validade_token, '/', '', true, true);

                $response = [
                    'id' => $user->id,
                    'token' => $token,
                    'validade_token' => date("Y-m-d H:i:s", $validade_token)
                ];

                $this->salvarTokenUsuario($response);
            }

            $_SESSION["user_id"] = $user->id;

            return ['success' => true, 'message' => 'Login realizado com sucesso.', 'title' => 'Sucesso', 'dados' => $user];
        }
    }

    public function salvarTokenUsuario($dados)
    {
        $pdo = $this->database->getPDO();
        $sql = "UPDATE $this->table 
                SET remember_token = :token, validade_token = :validade_token
                WHERE id = :id";

        $stmt = $pdo->prepare($sql);

        $stmt->bindParam(':id', $dados['id'], PDO::PARAM_INT);
        $stmt->bindParam(':token', $dados['token'], PDO::PARAM_STR);
        $stmt->bindParam(':validade_token', $dados['validade_token'], PDO::PARAM_STR);
        $stmt->execute();
        
    }

    public function limparTokenUsuario($id)
    {
        $pdo = $this->database->getPDO();
        $sql = "UPDATE $this->table 
                SET remember_token = '', validade_token = NULL
                WHERE id = :id";

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':id', $dados['id'], PDO::PARAM_INT);
        $stmt->execute();
        
    }

    public function recuperaUsuarioPorEmail($email)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table WHERE email = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $email, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    public function recuperaTodosUsuario($registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table ORDER BY nome ASC LIMIT ? OFFSET ?";
        $stmt = $pdo->prepare($sql);

        $stmt->bindParam(1, $registrosPorPagina, PDO::PARAM_INT);
        $stmt->bindParam(2, $offset, PDO::PARAM_INT); 
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function recuperarTodosPaginadoAtivosUsuario($registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table 
                WHERE ativo = 1 
                ORDER BY nome ASC 
                LIMIT ? OFFSET ?";
                
        $stmt = $pdo->prepare($sql);

        $stmt->bindParam(1, $registrosPorPagina, PDO::PARAM_INT);
        $stmt->bindParam(2, $offset, PDO::PARAM_INT); 
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function recuperarTodosPaginadoUsuario($registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table ORDER BY nome ASC LIMIT ? OFFSET ?";
        $stmt = $pdo->prepare($sql);

        $stmt->bindParam(1, $registrosPorPagina, PDO::PARAM_INT);
        $stmt->bindParam(2, $offset, PDO::PARAM_INT); 
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function obterCaminhoImagemUsuario($idUsuario)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT avatar FROM $this->table WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $idUsuario, PDO::PARAM_INT);
        $stmt->execute();

        $resultado = $stmt->fetch(PDO::FETCH_OBJ);

        return $resultado ? $resultado->avatar : null;
    }

    public function salvarImagemNoBanco($idUsuario, $caminho) 
    {
        $pdo = $this->database->getPDO();
        $caminhoBase = realpath("../public/storage/users");
        $caminhoAntigo = $caminhoBase . DIRECTORY_SEPARATOR . urlencode($this->obterCaminhoImagemUsuario($idUsuario));

        if ($caminhoExistente && file_exists($caminhoExistente)) {
            unlink($caminhoExistente);
        }

        $sql = "UPDATE $this->table SET avatar = ? WHERE id = ?";

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $caminho, PDO::PARAM_STR);
        $stmt->bindParam(2, $idUsuario, PDO::PARAM_INT);

        $stmt->execute();

        return true;
    }

    public function alterarSenhaUsuario($id, $senha)
    {
        $pdo = $this->database->getPDO();
        $sql = "UPDATE $this->table SET senha = ? WHERE id = ?";

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $senha, PDO::PARAM_STR);
        $stmt->bindParam(2, $id, PDO::PARAM_INT);

        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        return ['success' => true, 'message' => 'Senha alterada com sucesso.', 'title' => 'Sucesso'];
    }

    public function excluiUsuario($id)
    {
        $pdo = $this->database->getPDO();
        $sql = "DELETE FROM $this->table WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $id, PDO::PARAM_INT);        
        $stmt->execute();
    }

    public function excluirUsuario($id)
    {
        $pdo = $this->database->getPDO();
        $sql = "DELETE FROM $this->table WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        
        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        return ['success' => true, 'message' => 'Usuário removido com sucesso.', 'title' => 'Sucesso'];
    }

    public function habilitarDesabilitarUsuario($id, $value)
    {
        $pdo = $this->database->getPDO();
        $sql = "UPDATE $this->table SET ativo = ? WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $value, PDO::PARAM_INT);
        $stmt->bindParam(2, $id, PDO::PARAM_INT);
        
        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        return ['success' => true, 'message' => 'Usuário editado com sucesso.', 'title' => 'Sucesso'];
    }

    public function contaTodosRegistrosUsuario()
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT COUNT(*) FROM $this->table";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    public function contaTodosRegistrosAtivosUsuario()
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT COUNT(*) FROM $this->table 
                WHERE ativo = 1";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    public function contaRegistrosBuscadosUsuario($termoBusca)
    {
        $palavrasChave = explode(' ', $termoBusca);
        $condicoes = [];

        foreach ($palavrasChave as $index => $palavra) {
            $palavraSemAcento = $this->globalsModel->removerAcentos($palavra);

            $condicoes[] = "(nome LIKE :busca{$index} OR nome LIKE :buscaSemAcento{$index}
                            OR sobrenome LIKE :busca{$index} OR sobrenome LIKE :buscaSemAcento{$index}
                            OR email LIKE :busca{$index} OR email LIKE :buscaSemAcento{$index})";
        }

        $pdo = $this->database->getPDO();

        $sql = "SELECT COUNT(*) FROM $this->table";

        if (!empty($termoBusca)) {
            $sql .= " AND " . implode(" OR ", $condicoes) . "";
        }

        $stmt = $pdo->prepare($sql);

        if ($stmt) {

            if (!empty($termoBusca)) {
                foreach ($palavrasChave as $index => $palavra) {
                    $palavrasChaveSemAcento = $this->globalsModel->removerAcentos($palavra);
                
                    $buscaParam = "%{$palavra}%";
                    $buscaSemAcentoParam = "%{$palavrasChaveSemAcento}%";
                
                    $stmt->bindParam(":busca{$index}", $buscaParam, PDO::PARAM_STR);
                    $stmt->bindParam(":buscaSemAcento{$index}", $buscaParam, PDO::PARAM_STR);
                }
            }

            $stmt->execute();

            return $stmt->fetchColumn();
        } else {
            $errorInfo = $pdo->errorInfo();
            return ['success' => false, 'message' => "Erro na execução do processamento: " . $stmt->errorInfo()[2], 'title' => 'Falha no Processamento'];
        }
    }

    public function verificarCookie($userId, $userToken) 
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table 
                WHERE id = :userId 
                AND remember_token = :userToken 
                AND validade_token > NOW()";

        $stmt = $pdo->prepare($sql);

        $stmt->bindParam(":userId", $userId, PDO::PARAM_INT);
        $stmt->bindParam(":userToken", $userToken, PDO::PARAM_STR);

        if ($stmt->execute()) {
            $result = $stmt->fetchColumn();

            if ($result) {
                return true;
            }
        }

        return false;
    }
}

function gerarTokenUnico() {    
    return bin2hex(random_bytes(32));
}
