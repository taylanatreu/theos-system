<?php

namespace App\Models;

use Databases\Database;
use PDO;

class GrupoUsuarioModel
{
    private $database;
    private $globalsModel;

    public function __construct(Database $database, GlobalsModel $globalsModel)
    {
        $this->database = $database;
        $this->table = "grupos_usuarios";
        $this->globalsModel = $globalsModel;
    }

    public function inserirGrupoUsuario($dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $colunas = implode(', ', array_keys($dados));
        $valores = implode(', ', array_fill(0, count($dados), '?'));

        $query = "INSERT INTO $this->table ($colunas) VALUES ($valores)";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            return ['success' => false, 'message' => "Erro na execução do processamento: " . $stmt->errorInfo()[2], 'title' => 'Falha no Processamento'];
        } else {
            return ['success' => true, 'message' => "Usuário adicionado no grupo.", 'title' => 'Sucesso'];
        }
    }

    public function removerGrupoUsuario($dados)
    {
        $pdo = $this->database->getPDO();
        $sql = "DELETE FROM $this->table WHERE userId = ? AND grupoId = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $dados["userId"], PDO::PARAM_INT);
        $stmt->bindParam(2, $dados["grupoId"], PDO::PARAM_INT);
        $stmt->execute();

        if (!$stmt->execute()) {
            return ['success' => false, 'message' => "Erro na execução do processamento: " . $stmt->errorInfo()[2], 'title' => 'Falha no Processamento'];
        } else {
            return ['success' => true, 'message' => "Usuário removido do grupo.", 'title' => 'Sucesso'];
        }
    }

    public function verificaUsuario($userId, $grupoId) 
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table WHERE userId = ? AND grupoId = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $userId, PDO::PARAM_INT);
        $stmt->bindParam(2, $grupoId, PDO::PARAM_INT);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        return $count;
    }

    public function recuperarTodosGrupoUsuarioPorUsuario($userId)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT gu.*, g.name as name FROM $this->table as gu
                INNER JOIN grupos as g
                ON gu.grupoId = g.id
                WHERE gu.userId = ?
                ORDER BY g.name ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $userId, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    
}
