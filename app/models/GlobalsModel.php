<?php

namespace App\Models;

class GlobalsModel 
{
    public function __construct() 
    {

    }

    public function removerAcentos($string) 
    {
        $acentos = array('á', 'à', 'ã', 'â', 'é', 'è', 'ê', 'í', 'ì', 'î', 'ó', 'ò', 'õ', 'ô', 'ú', 'ù', 'û', 'ç', 'ñ', '.', '-', '$', ' ','(', ')', '[', ']', '#', '/');
        $semAcento = array('a', 'a', 'a', 'a', 'e', 'e', 'e', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'c', 'n', '', '', '', '', '', '', '', '', '', '', '');
        
        return str_replace($acentos, $semAcento, $string);
    }

    public function transformAcentos($string) 
    {
        $acentos = array('á', 'à', 'ã', 'â', 'é', 'è', 'ê', 'í', 'ì', 'î', 'ó', 'ò', 'õ', 'ô', 'ú', 'ù', 'û', 'ç', 'ñ', '.', '-', '$', ' ','(', ')', '[', ']', '#', '/');
        $semAcento = array('a', 'a', 'a', 'a', 'e', 'e', 'e', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'c', 'n', '', '', '', '', '', '', '', '', '', '', '');
        
        return str_replace($acentos, $semAcento, $string);
    }

    public function toDecimal($number) 
    {        
        return number_format($number, 2, ',', '.');
    }

    public function toNumber($number) 
    {        
        return number_format($number, 2, '.', '');
    }

    function formataData($data) {  
		if ($data!='') {   
			return (substr($data,8,2).'/'.substr($data,5,2).'/'.substr($data,0,4));   
		}   
			else { return ''; }   
	}

    function formataDataBD($data) {  
		if ($data!='') {   
			return (substr($data,6,4).'-'.substr($data,3,2).'-'.substr($data,0,2));   
		}   
			else { return ''; }   
	}
}
