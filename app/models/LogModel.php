<?php

namespace App\Models;

use Databases\Database;
use PDO;

class LogModel
{
    private $database;
    private $globalsModel;

    public function __construct(Database $database, GlobalsModel $globalsModel)
    {
        $this->database = $database;
        $this->table = "logs";
        $this->globalsModel = $globalsModel;
    }

    public function buscarRegistrosLog($termoBusca, $registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $palavrasChave = explode(' ', $termoBusca);
        $condicoes = [];

        foreach ($palavrasChave as $index => $palavra) {
            $palavraSemAcento = $this->globalsModel->removerAcentos($palavra);

            $condicoes[] = "(u.nome LIKE :busca{$index} OR u.nome LIKE :buscaSemAcento{$index} 
                            OR u.sobrenome LIKE :busca{$index} OR u.sobrenome LIKE :buscaSemAcento{$index} 
                            OR l.descricao LIKE :busca{$index} OR l.descricao LIKE :buscaSemAcento{$index} 
                            OR l.status LIKE :busca{$index} OR l.status LIKE :buscaSemAcento{$index})";
        }

        if (!empty($termoBusca)) {
            $condicaoBusca = " AND " . implode(" OR ", $condicoes) . "";
        } else {
            $condicaoBusca = "";
        }

        $sql = "SELECT COUNT(*) FROM $this->table as l 
                INNER JOIN usuarios as u 
                ON u.id = l.userId 
                WHERE u.user = :userId
                $condicoesBusca
                ORDER BY l.created_at DESC
                LIMIT :registrosPorPagina OFFSET :offset";

        $stmt = $pdo->prepare($sql);

        if ($stmt) {
            
            if (!empty($termoBusca)) {
                foreach ($palavrasChave as $index => $palavra) {
                    $palavraSemAcento = $this->globalsModel->removerAcentos($palavra);
                
                    $buscaParam = "%{$palavra}%";
                    $buscaSemAcentoParam = "%{$palavraSemAcento}%";
                
                    $stmt->bindParam(":busca{$index}", $buscaParam, PDO::PARAM_STR);
                    $stmt->bindParam(":buscaSemAcento{$index}", $buscaSemAcentoParam, PDO::PARAM_STR);
                }
            }

            $stmt->bindParam(':userId', $_SESSION["user_id"], PDO::PARAM_INT);
            $stmt->bindParam(':registrosPorPagina', $registrosPorPagina, PDO::PARAM_INT);
            $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
            $stmt->execute();

            $resultados = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $resultados;
        } else {
            $errorInfo = $pdo->errorInfo();
            return ['success' => false, 'message' => "Erro na execução do processamento: " . $stmt->errorInfo()[2], 'title' => 'Falha no Processamento'];
        }
        
        $pdo = $this->database->getPDO();
        $sql = "SELECT l.*, u.nome as nome, u.sobrenome as sobrenome FROM $this->table as l
                INNER JOIN usuarios as u
                ON l.userId = u.id
                WHERE l.userId = ?
                AND l.descricao LIKE ? 
                OR l.status LIKE ? 
                ORDER BY l.created_at DESC 
                LIMIT ? OFFSET ?";

        $stmt = $pdo->prepare($sql);
        
        $busca = "%" . $termoBusca . "%";

        $stmt->bindParam(1, $_SESSION["user_id"], PDO::PARAM_INT);
        $stmt->bindParam(2, $busca, PDO::PARAM_STR);
        $stmt->bindParam(3, $busca, PDO::PARAM_STR);
        $stmt->bindParam(4, $registrosPorPagina, PDO::PARAM_INT);
        $stmt->bindParam(5, $offset, PDO::PARAM_INT); 
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function recuperarTodosLog()
    {
        $pdo = $this->database->getPDO();
        $query = "SELECT * FROM $this->table ORDER BY created_at DESC";
        $stmt = $pdo->prepare($sql);
        $stmt = execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function recuperarTodosPaginadoLog($registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT l.*, u.nome as nome, u.sobrenome as sobrenome FROM $this->table as l
                INNER JOIN usuarios as u
                ON l.userId = u.id
                WHERE l.userId = ?
                ORDER BY l.created_at DESC 
                LIMIT ? OFFSET ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $_SESSION["user_id"], PDO::PARAM_INT);
        $stmt->bindParam(2, $registrosPorPagina, PDO::PARAM_INT);
        $stmt->bindParam(3, $offset, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function inserirLog($dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $colunas = implode(', ', array_keys($dados));
        $valores = implode(', ', array_fill(0, count($dados), '?'));

        $query = "INSERT INTO $this->table ($colunas) VALUES ($valores)";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        return $stmt->execute();
    }

    public function contaTodosRegistrosLog()
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT COUNT(*) FROM $this->table";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    public function contaRegistrosBuscadosLog($termoBusca)
    {
        $palavrasChave = explode(' ', $termoBusca);
        $condicoes = [];

        foreach ($palavrasChave as $index => $palavra) {
            $palavraSemAcento = $this->globalsModel->removerAcentos($palavra);

            $condicoes[] = "(u.nome LIKE :busca{$index} OR u.nome LIKE :buscaSemAcento{$index} 
                            OR u.sobrenome LIKE :busca{$index} OR u.sobrenome LIKE :buscaSemAcento{$index} 
                            OR l.descricao LIKE :busca{$index} OR l.descricao LIKE :buscaSemAcento{$index} 
                            OR l.status LIKE :busca{$index} OR l.status LIKE :buscaSemAcento{$index})";
        }

        $pdo = $this->database->getPDO();

        $sql = "SELECT COUNT(*) FROM $this->table as l 
                INNER JOIN usuarios as u 
                ON u.id = l.userId 
                WHERE u.user = :userId";

        if (!empty($termoBusca)) {
            $sql .= " AND " . implode(" OR ", $condicoes) . "";
        }

        $stmt = $pdo->prepare($sql);

        if ($stmt) {

            if (!empty($termoBusca)) {
                foreach ($palavrasChave as $index => $palavra) {
                    $palavrasChaveSemAcento = $this->globalsModel->removerAcentos($palavra);
                
                    $buscaParam = "%{$palavra}%";
                    $buscaSemAcentoParam = "%{$palavrasChaveSemAcento}%";
                
                    $stmt->bindParam(":busca{$index}", $buscaParam, PDO::PARAM_STR);
                    $stmt->bindParam(":buscaSemAcento{$index}", $buscaParam, PDO::PARAM_STR);
                }
            }

            $stmt->bindParam(':userId', $_SESSION["user_id"], PDO::PARAM_INT);

            $stmt->execute();

            return $stmt->fetchColumn();
        } else {
            $errorInfo = $pdo->errorInfo();
            return ['success' => false, 'message' => "Erro na execução do processamento: " . $stmt->errorInfo()[2], 'title' => 'Falha no Processamento'];
        }
    }
}
