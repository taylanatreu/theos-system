<?php

namespace App\Models;

use Databases\Database;
use PDO;

class GrupoModel
{
    private $database;
    private $globalsModel;

    public function __construct(Database $database, GlobalsModel $globalsModel)
    {
        $this->database = $database;
        $this->table = "grupos";
        $this->globalsModel = $globalsModel;
    }

    public function buscarRegistrosGrupo($termoBusca, $registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $palavrasChave = explode(' ', $termoBusca);
        $condicoes = [];

        foreach ($palavrasChave as $index => $palavra) {
            $palavraSemAcento = $this->globalsModel->removerAcentos($palavra);

            $condicoes[] = "(name LIKE :busca{$index} OR name LIKE :buscaSemAcento{$index} 
                            OR descricao LIKE :busca{$index} OR descricao LIKE :buscaSemAcento{$index})";
        }

        if (!empty($termoBusca)) {
            $condicaoBusca = " AND " . implode(" OR ", $condicoes) . "";
        } else {
            $condicaoBusca = "";
        }

        $sql = "SELECT * FROM $this->table
                WHERE 1=1 
                $condicaoBusca 
                ORDER BY name ASC
                LIMIT :registrosPorPagina OFFSET :offset";

        $stmt = $pdo->prepare($sql);

        if ($stmt) {
            
            if (!empty($termoBusca)) {
                foreach ($palavrasChave as $index => $palavra) {
                    $palavraSemAcento = $this->globalsModel->removerAcentos($palavra);
                
                    $buscaParam = "%{$palavra}%";
                    $buscaSemAcentoParam = "%{$palavraSemAcento}%";
                
                    $stmt->bindParam(":busca{$index}", $buscaParam, PDO::PARAM_STR);
                    $stmt->bindParam(":buscaSemAcento{$index}", $buscaSemAcentoParam, PDO::PARAM_STR);
                }
            }

            $stmt->bindParam(':registrosPorPagina', $registrosPorPagina, PDO::PARAM_INT);
            $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
            $stmt->execute();

            $resultados = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $resultados;
        } else {
            $errorInfo = $pdo->errorInfo();
            return ['success' => false, 'message' => "Erro na execução do processamento: " . $stmt->errorInfo()[2], 'title' => 'Falha no Processamento'];
        }
    }

    public function recuperarGrupo($id)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    public function recuperarTodosGrupo()
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table ORDER BY name ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function recuperarTodosPaginadoGrupo($registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table ORDER BY name ASC LIMIT ? OFFSET ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $registrosPorPagina, PDO::PARAM_INT);
        $stmt->bindParam(2, $offset, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function inserirGrupo($dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $colunas = implode(', ', array_keys($dados));
        $valores = implode(', ', array_fill(0, count($dados), '?'));

        $query = "INSERT INTO $this->table ($colunas) VALUES ($valores)";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }

        $grupoId = $this->database->getPDO()->lastInsertId();
        $grupo = $this->recuperarGrupo($grupoId);
    
        return ['success' => true, 'message' => 'Grupo inserido com sucesso.', 'title' => 'Sucesso', 'dados' => $grupo];
    }

    public function editarGrupo($id, $dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $atualizacoes = array();
        foreach ($dados as $coluna => $valor) {
            $atualizacoes[] = "$coluna = ?";
        }

        $atualizacoesStr = implode(', ', $atualizacoes);

        $query = "UPDATE $this->table SET $atualizacoesStr WHERE id = ?";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        $valoresParametros[] = $id;

        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }

        $grupo = $this->recuperarGrupo($id);
    
        return ['success' => true, 'message' => 'Grupo editado com sucesso.', 'title' => 'Sucesso', 'dados' => $grupo];
    }


    public function excluirGrupo($id)
    {
        $pdo = $this->database->getPDO();
        $sql = "DELETE FROM $this->table WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        
        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        return ['success' => true, 'message' => 'Grupo removido com sucesso.', 'title' => 'Sucesso'];
    }

    public function habilitarDesabilitarGrupo($id, $value)
    {
        $pdo = $this->database->getPDO();
        $sql = "UPDATE $this->table SET ativo = ? WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $value, PDO::PARAM_INT);
        $stmt->bindParam(2, $id, PDO::PARAM_INT);
        
        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        return ['success' => true, 'message' => 'Grupo editado com sucesso.', 'title' => 'Sucesso'];
    }

    public function contaTodosRegistrosGrupo()
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT COUNT(*) FROM $this->table";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    public function contaRegistrosBuscadosGrupo($termoBusca)
    {
        $palavrasChave = explode(' ', $termoBusca);
        $condicoes = [];

        foreach ($palavrasChave as $index => $palavra) {
            $palavraSemAcento = $this->globalsModel->removerAcentos($palavra);

            $condicoes[] = "(name LIKE :busca{$index} OR name LIKE :buscaSemAcento{$index}
                            OR descricao LIKE :busca{$index} OR descricao LIKE :buscaSemAcento{$index})";
        }

        $pdo = $this->database->getPDO();

        $sql = "SELECT COUNT(*) FROM $this->table 
                WHERE 1=1";

        if (!empty($termoBusca)) {
            $sql .= " AND " . implode(" OR ", $condicoes) . "";
        }

        $stmt = $pdo->prepare($sql);

        if ($stmt) {

            if (!empty($termoBusca)) {
                foreach ($palavrasChave as $index => $palavra) {
                    $palavrasChaveSemAcento = $this->globalsModel->removerAcentos($palavra);
                
                    $buscaParam = "%{$palavra}%";
                    $buscaSemAcentoParam = "%{$palavrasChaveSemAcento}%";
                
                    $stmt->bindParam(":busca{$index}", $buscaParam, PDO::PARAM_STR);
                    $stmt->bindParam(":buscaSemAcento{$index}", $buscaParam, PDO::PARAM_STR);
                }
            }

            $stmt->execute();

            return $stmt->fetchColumn();
        } else {
            $errorInfo = $pdo->errorInfo();
            return ['success' => false, 'message' => "Erro na execução do processamento: " . $stmt->errorInfo()[2], 'title' => 'Falha no Processamento'];
        }
    }
}
