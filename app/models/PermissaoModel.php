<?php

namespace App\Models;

use Databases\Database;
use PDO;

class PermissaoModel
{
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
        $this->table = "permissoes";
    }

    public function buscarRegistrosPermissao($termoBusca, $registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table WHERE descricao LIKE ? ORDER BY descricao ASC LIMIT ? OFFSET ?";
        $stmt = $pdo->prepare($sql);
        $busca = "%" . $termoBusca . "%";

        $stmt->bindParam(1, $busca, PDO::PARAM_STR);
        $stmt->bindParam(2, $registrosPorPagina, PDO::PARAM_INT);
        $stmt->bindParam(3, $offset, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function recuperarPermissao($id)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table WHERE id = ?";
        $stmt = $pdo->prepare($sql); 
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ);
    }


    public function recuperarTodosPermissao()
    {
        $query = "SELECT * FROM $this->table ORDER BY descricao ASC";
        $stmt = $this->database->getPDO()->query($query);

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function recuperarTodosPaginadoPermissao($registrosPorPagina, $offset)
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT * FROM $this->table ORDER BY descricao ASC LIMIT ? OFFSET ?";
        $stmt = $pdo->prepare($sql);

        $stmt->bindParam(1, $registrosPorPagina, PDO::PARAM_INT);
        $stmt->bindParam(2, $offset, PDO::PARAM_INT); 
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function inserirPermissao($dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $colunas = implode(', ', array_keys($dados));
        $valores = implode(', ', array_fill(0, count($dados), '?'));

        $query = "INSERT INTO $this->table ($colunas) VALUES ($valores)";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }

        $permissaoId = $this->database->getPDO()->lastInsertId();
        $permissao = $this->recuperarPermissao($permissaoId);
    
        return ['success' => true, 'message' => 'Permissão inserida com sucesso.', 'title' => 'Sucesso', 'dados' => $permissao];
    }

    public function editarPermissao($id, $dados)
    {
        if (!is_array($dados) || empty($dados)) {
            return false;
        }

        $atualizacoes = array();
        foreach ($dados as $coluna => $valor) {
            $atualizacoes[] = "$coluna = ?";
        }

        $atualizacoesStr = implode(', ', $atualizacoes);

        $query = "UPDATE $this->table SET $atualizacoesStr WHERE id = ?";

        $stmt = $this->database->getPDO()->prepare($query);

        $valoresParametros = array_values($dados);
        $valoresParametros[] = $id;

        foreach ($valoresParametros as $key => $valor) {
            $stmt->bindValue($key + 1, $valor);
        }

        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }

        $permissao = $this->recuperarPermissao($id);
    
        return ['success' => true, 'message' => 'Permissão editada com sucesso.', 'title' => 'Sucesso', 'dados' => $permissao];
    }


    public function excluirPermissao($id)
    {
        $pdo = $this->database->getPDO();
        $sql = "DELETE FROM $this->table WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        
        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        return ['success' => true, 'message' => 'Permissão removida com sucesso.', 'title' => 'Sucesso'];
    }

    public function habilitarDesabilitarPermissao($id, $value)
    {
        $pdo = $this->database->getPDO();
        $sql = "UPDATE $this->table SET ativo = ? WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $value, PDO::PARAM_INT);
        $stmt->bindParam(2, $id, PDO::PARAM_INT);
        
        if (!$stmt->execute()) {
            return ['success' => false,'title' => 'Falha no Processamento', 'message' => 'Erro na execução da consulta: ' . $stmt->errorInfo()[2]];
        }
    
        return ['success' => true, 'message' => 'Permissão editada com sucesso.', 'title' => 'Sucesso'];
    }

    public function contaTodosRegistrosPermissao()
    {
        $pdo = $this->database->getPDO();
        $sql = "SELECT COUNT(*) FROM $this->table";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    public function contaRegistrosBuscadosPermissao($termoBusca)
    {
        $pdo = $this->database->getPDO();
        
        $sql = "SELECT COUNT(*) FROM $this->table WHERE descricao LIKE ? ORDER BY descricao ASC";
        $stmt = $pdo->prepare($sql);
        
        $busca = "%" . $termoBusca . "%";

        $stmt->bindParam(1, $busca, PDO::PARAM_STR);
        
        $stmt->execute();
        return $stmt->fetchColumn();
    }
}
