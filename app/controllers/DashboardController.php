<?php

namespace App\Controllers;

use Databases\Database;
use PDO;
use App\Models\GlobalsModel;
use App\Models\UsuarioModel;

require_once __DIR__ . '/Controller.php';

class DashboardController extends Controller
{
    private $usuarioModel;
    private $globalsModel;

    public function __construct(Database $database)
    {
        parent::__construct($database);
        $this->globalsModel = new GlobalsModel();
        $this->usuarioModel = new UsuarioModel($database, $this->globalsModel);
    }

    public function index()
    {
        include '../app/views/dashboard.php';
    }
}
