<?php

namespace App\Controllers;

use Databases\Database;

class Controller
{
    protected $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }
}