<?php

namespace App\Controllers;

use Databases\Database;
use PDO;
use App\Models\GlobalsModel;
use App\Models\ClienteModel;
use App\Models\UsuarioModel;

require_once __DIR__ . '/Controller.php';

class ClienteController extends Controller
{
    private $globalsModel;
    private $clienteModel;
    private $usuarioModel;

    public function __construct(Database $database)
    {
        parent::__construct($database);
        $this->globalsModel = new GlobalsModel();
        $this->clienteModel = new ClienteModel($database, $this->globalsModel);
        $this->usuarioModel = new UsuarioModel($database, $this->globalsModel);
    }

    public function index()
    {
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->clienteModel->contaTodosRegistrosCliente();
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;
        $clientes = $this->clienteModel->recuperarTodosPaginadoCliente($registrosPorPagina, $offset);
        
        include '../app/views/clientes/lista-clientes.php';
    }

    public function create()
    {
        $clienteId = isset($_GET['clienteId']) ? $_GET['clienteId'] : "";

        if ($clienteId) {
            $cliente = $this->clienteModel->recuperaCliente($clienteId);
        } else {
            $cliente = [];
        }

        include '../app/views/clientes/create.php';
    }

    public function buscaDados()
    {
        $nomeCampo = $_POST["nomeCampo"];
        $valorCampo = $_POST["valorCampo"];

        $resultado = $this->clienteModel->recuperaDadosPesquisa($nomeCampo, $valorCampo);

        header('Content-Type: application/json');
        echo json_encode($resultado);
        exit(); 
    }
    
    public function insere()
    {
        $dadosRequest = $_POST;
        $clienteId = isset($dadosRequest['clienteId']) ? $dadosRequest['clienteId'] : "";
        
    }

    public function detalhes()
    {
        $clienteId = isset($_GET['clienteId']) ? $_GET['clienteId'] : "";

        $cliente = $this->clienteModel->recuperaCliente($clienteId); 

        include '../app/views/clientes/details.php'; 
    }

    public function exclui()
    {
        $id = $_GET['id'];        
        $result = $this->clienteModel->excluirCliente($id);
        
        header('Content-Type: application/json');
        echo json_encode($result);
        exit();        
    }

    public function habilitar()
    {
        $id = $_GET['id'];
        $value = 1;
        
        $cliente = $this->clienteModel->recuperaCliente($id);                
        $result = $this->usuarioModel->habilitarDesabilitarUsuario($cliente->userId, $value);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    public function desabilitar()
    {
        $id = $_GET['id'];
        $value = 0;

        $cliente = $this->clienteModel->recuperaCliente($id);                
        $result = $this->usuarioModel->habilitarDesabilitarUsuario($cliente->userId, $value);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }
}