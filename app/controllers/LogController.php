<?php

namespace App\Controllers;

use Databases\Database;
use PDO;
use App\Models\GlobalsModel;
use App\Models\LogModel;
use App\Models\UsuarioModel;

require_once __DIR__ . '/Controller.php';

class LogController extends Controller
{
    private $logModel;
    private $usuarioModel;
    private $globalsModel;

    public function __construct(Database $database)
    {
        parent::__construct($database);
        $this->globalsModel = new GlobalsModel();
        $this->logModel = new LogModel($this->database, $this->globalsModel);
        $this->usuarioModel = new UsuarioModel($this->database, $this->globalsModel);
    }

    public function index()
    {
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->logModel->contaTodosRegistrosLog();
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;
        $logs = $this->logModel->recuperarTodosPaginadoLog($registrosPorPagina, $offset);

        include '../app/views/seguranca/lista-logs.php';
    }

    public function pesquisa()
    {
        $termoBusca = isset($_GET['termo_busca']) ? $_GET['termo_busca'] : '';
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->logModel->contaRegistrosBuscadosLog($termoBusca);
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;

        $results = $this->logModel->buscarRegistrosLog($termoBusca, $registrosPorPagina, $offset);

        $html = '';
        $x = 1;

        if (isset($results) && is_array($results) && !empty($results)) {
            foreach ($results as $resultado) {

                if ($resultado->status == 'error') { 
                    $bstatus = "danger"; 
                } else { 
                    $bstatus = "success"; 
                }

                $created_at = date("d/m/Y H:i", strtotime($resultado->created_at));

                $html .= '<tr id="registro'. $resultado->id .'">';
                $html .= '<td class="text-center" style="width: 10%;">' . $x . '</td>';
                $html .= '<td class="text-center">' . $resultado->nome . ' ' . $resultado->sobrenome . '</td>';
                $html .= '<td class="text-center">' . $resultado->descricao . '</td>';
                $html .= '<td class="text-center"><div class="badge badge-' . $bstatus . '">' . $resultado->status . '</div></td>';
                $html .= '<td class="text-center">' . $created_at . '</td>';
                $html .= '</tr>';
                $x ++; 
            } 
        } else {
            $html .= '<tr>';
            $html .= '<td class="text-center" colspan="5">Nenhum registro encontrado.</td>';
            $html .= '</tr>';
        }

        $htmlPagination = '';
        $htmlPagination .= 'Exibindo ' . $registrosPorPagina . ' registros da página ' . $paginaAtual . '/' . $totalPaginas . ' de um total de ' . $totalRegistros . ' registros.'; 

        $response = [
            "dados" => $html,
            'pagination' => $htmlPagination
        ];

        header('Content-Type: application/json');
        echo json_encode($response);
        exit();
    }

    public function insere()
    {
        $descricao = $_POST['descricao'];
        $status = $_POST['status'];
        $userId = $_POST['userId'];

        $request = [
            'descricao' => $descricao,
            'status' => $status,
            'userId' => $userId,
            'created_at' => date("Y-m-d H:i:s")
        ];

        return $this->logModel->inserirLog($request);       
    }
}
