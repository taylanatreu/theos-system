<?php

namespace App\Controllers;

use Databases\Database;
use PDO;
use App\Models\GlobalsModel;
use App\Models\UsuarioModel;
use App\Models\LogModel;

require_once __DIR__ . '/Controller.php';

class LoginController extends Controller
{
    private $usuarioModel;
    private $logModel;
    private $globalsModel;

    public function __construct(Database $database)
    {
        parent::__construct($database);
        $this->globalsModel = new GlobalsModel();
        $this->usuarioModel = new UsuarioModel($database, $this->globalsModel);
        $this->logModel = new LogModel($database, $this->globalsModel);
    }

    public function index()
    {
        include '../app/views/login.php';
    }

    public function autenticacao()
    {
        $login = $_POST['login'];
        $senha = $_POST['senha'];
        $remember = isset($_POST['remember']) ? $_POST['remember'] : 0;
        
        $result = $this->usuarioModel->autenticacao($login, $senha, $remember);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    public function logout()
    {
        $this->usuarioModel->limparTokenUsuario($_SESSION['user_id']);

        session_destroy();
        setcookie('remember_token', '', time() - 3600, '/', '', true, true);

        header("X-Logout: Success");
        header("Location: /login");
        exit;
    }
}
