<?php

namespace App\Controllers;

use Databases\Database;
use PDO;
use App\Models\GlobalsModel;
use App\Models\GrupoPermissaoModel;
use App\Models\GrupoModel;
use App\Models\PermissaoModel;

require_once __DIR__ . '/Controller.php';

class GrupoPermissaoController extends Controller
{
    private $grupoPermissaoModel;
    private $globalsModel;

    public function __construct(Database $database)
    {
        parent::__construct($database);
        $this->globalsModel = new GlobalsModel();
        $this->grupoPermissaoModel = new GrupoPermissaoModel($this->database, $this->globalsModel);
        $this->grupoModel = new GrupoModel($this->database, $this->globalsModel);
        $this->permissaoModel = new PermissaoModel($this->database);
    }

    public function index()
    {
        $grupoId = $_GET["grupoId"];
        $grupo = $this->grupoModel->recuperarGrupo($grupoId);

        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->permissaoModel->contaTodosRegistrosPermissao();
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;
        $permissoes = $this->permissaoModel->recuperarTodosPaginadoPermissao($registrosPorPagina, $offset);

        include '../app/views/seguranca/lista-grupos-permissoes.php';
    }

    public function refresh()
    {
        $grupoId = $_GET["grupoId"];
        $grupo = $this->grupoModel->recuperarGrupo($grupoId);
        
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->permissaoModel->contaTodosRegistrosPermissao();
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;
        $result = $this->permissaoModel->recuperarTodosPaginadoPermissao($registrosPorPagina, $offset);

        $html = '';
        $x = 1;
        if (isset($result) && is_array($result) && !empty($result)) {
            foreach ($result as $resultado) {

                $checked = $this->verificacao($resultado->id, $grupoId) ? 'checked' : '';

                if ($resultado->ativo == 1) {
                    $status = '<i class="far fa-check-square"></i>';
                    $target = '#confirmacaoDesabilitaModal';
                    $title = 'Desabilitar';
                    $icon = 'fas fa-ban';
                    $class = 'warning';
                } else {
                    $status = '<i class="fas fa-ban"></i>';
                    $target = '#confirmacaoHabilitaModal';
                    $title = 'Habilitar';
                    $icon = 'fas fa-check';
                    $class = 'success';
                }

                $html .= '<tr id="registro'. $resultado->id .'">';
                $html .= '<td class="text-center">' . $x . '</td>';
                $html .= '<td class="text-center">' . $resultado->descricao . '</td>';
                $html .= '<td class="text-center">' . $status . '</td>';
                $html .= '<td class="text-center">
                            <button 
                                type="button" 
                                class="btn btn-xs btn-info" 
                                title="Editar Permissão" 
                                onclick="editarRegistro(' . $resultado->id . ')">
                                <i class="fas fa-edit"></i>
                            </button> 

                            <a 
                                href="#" 
                                class="btn btn-xs btn-'. $class .'" 
                                data-toggle="modal" 
                                data-target="'. $target . '" 
                                data-id="' . $resultado->id . '"
                                data-grupo="' . $grupoId . '"
                                title="'. $title .' Permissão">
                                <i class="'. $icon . '"></i>
                            </a>

                            <a 
                                href="#" 
                                class="btn btn-xs btn-danger" 
                                title="Remover Permissão" 
                                data-toggle="modal" 
                                data-target="#confirmacaoExclusaoModal" 
                                data-id="' . $resultado->id . '"
                                data-grupo="' . $grupoId . '">
                                <i class="fas fa-trash"></i>
                            </a>
                        </td>';
                $html .= '<td class="text-center" style="width: 20%;">
                                <div class="form-group">
                                    <div class="custom-control custom-switch custom-switch-off-default custom-switch-on-success">
                                        <input 
                                            type="checkbox" 
                                            class="custom-control-input permissao-switch" 
                                            id="permissao-' . $resultado->id . '"
                                            data-permissao-id="' . $resultado->id . '" 
                                            data-grupo-id="' . $grupoId . '"
                                            ' .  $checked . '>
                                        <label class="custom-control-label" for="permissao-' . $resultado->id . '"></label>
                                    </div>
                                </div>
                            </td>';
                $html .= '</tr>';
                $x ++; 
            } 
        } else {
            $html .= '<tr>';
            $html .= '<td class="text-center" colspan="6">Nenhum registro encontrado.</td>';
            $html .= '</tr>';
        }        
        
        $htmlPagination = '';
        $htmlPagination .= 'Exibindo ' . $registrosPorPagina . ' registros da página ' . $paginaAtual . '/' . $totalPaginas . ' de um total de ' . $totalRegistros . ' registros.'; 

        $response = [
            "dados" => $html,
            'pagination' => $htmlPagination
        ];
        
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();   
    }

    public function pesquisaPermissao()
    {
        $grupoId = $_GET["grupoId"];
        $grupo = $this->grupoModel->recuperarGrupo($grupoId);
        
        $termoBusca = isset($_GET['termo_busca']) ? $_GET['termo_busca'] : '';
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->permissaoModel->contaRegistrosBuscadosPermissao($termoBusca);
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;

        $result = $this->permissaoModel->buscarRegistrosPermissao($termoBusca, $registrosPorPagina, $offset);

        $html = '';
        $x = 1;

        if (isset($result) && is_array($result) && !empty($result)) {
            foreach ($result as $resultado) {

                $checked = $this->verificacao($resultado->id, $grupoId) ? 'checked' : '';

                if ($resultado->ativo == 1) {
                    $status = '<i class="far fa-check-square"></i>';
                    $target = '#confirmacaoDesabilitaModal';
                    $title = 'Desabilitar';
                    $icon = 'fas fa-ban';
                    $class = 'warning';
                } else {
                    $status = '<i class="fas fa-ban"></i>';
                    $target = '#confirmacaoHabilitaModal';
                    $title = 'Habilitar';
                    $icon = 'fas fa-check';
                    $class = 'success';
                }

                $html .= '<tr>';
                $html .= '<td class="text-center" style="width: 10%;">' . $x . '</td>';
                $html .= '<td class="text-center">' . $resultado->descricao .'</td>';
                $html .= '<td class="text-center">' . $status . '</td>';
                $html .= '<td class="text-center">
                            <button 
                                type="button" 
                                class="btn btn-xs btn-info link" 
                                title="Editar Permissão" 
                                onclick="editarRegistro(' . $resultado->id . ')">
                                <i class="fas fa-edit"></i>
                            </button> 

                            <a 
                                href="#" 
                                class="btn btn-xs btn-'. $class .'" 
                                data-toggle="modal" 
                                data-target="'. $target . '" 
                                data-id="' . $resultado->id . '"
                                title="'. $title .' Permissão">
                                <i class="'. $icon . '"></i>
                            </a>

                            <a 
                                href="#" 
                                class="btn btn-xs btn-danger" 
                                title="Remover Permissão" 
                                data-toggle="modal" 
                                data-target="#confirmacaoExclusaoModal" 
                                data-id="' . $resultado->id . '">
                                <i class="fas fa-trash"></i>
                            </a>
                        </td>';
                $html .= '<td class="text-center" style="width: 20%;">
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-off-default custom-switch-on-success">
                                    <input 
                                        type="checkbox" 
                                        class="custom-control-input permissao-switch" 
                                        id="permissao-' . $resultado->id . '"
                                        data-permissao-id="' . $resultado->id . '" 
                                        data-grupo-id="' . $grupoId . '"
                                        ' .  $checked . '>
                                    <label class="custom-control-label" for="permissao-' . $resultado->id . '"></label>
                                </div>
                            </div>
                        </td>';
                $html .= '</tr>';
                $x ++; 
            } 
        } else {
            $html .= '<tr>';
            $html .= '<td class="text-center" colspan="5">Nenhum registro encontrado.</td>';
            $html .= '</tr>';
        }

        $htmlPagination = '';
        $htmlPagination .= 'Exibindo ' . $registrosPorPagina . ' registros da página ' . $paginaAtual . '/' . $totalPaginas . ' de um total de ' . $totalRegistros . ' registros.'; 

        $response = [
            "dados" => $html,
            'pagination' => $htmlPagination
        ];       
        
        header('Content-Type: application/json');
        echo json_encode($response);
        exit(); 
    }

    public function insere()
    {
        $grupoId = $_POST['grupoId'];
        $permissionId = $_POST['permissionId'];
        
        $request = [
            'grupoId' => $grupoId,
            'permissionId' => $permissionId
        ];

        if ($this->verificacao($permissionId, $grupoId)) {
            $remover = $this->grupoPermissaoModel->removerGrupoPermissao($request);

            if ($remover) {
                $response = [
                    'success' => true,
                    'message' => 'Permissão removida no grupo com sucesso',
                    'title' => 'Sucesso'
                ];
            } else {
                $response = [
                    'success' => false,
                    'message' => 'Falha ao remover um permissão do grupo',
                    'title' => 'Falhou'
                ];
            }
        } else {
            $grupos_usuarios = $this->grupoPermissaoModel->inserirGrupoPermissao($request);

            if ($grupos_usuarios) {
                $response = [
                    'success' => true,
                    'message' => 'Permissão do grupo inserida com sucesso',
                    'title' => 'Sucesso'
                ];
            } else {
                $response = [
                    'success' => false,
                    'message' => 'Falha ao inserir uma permissão do grupo',
                    'title' => 'Falhou'
                ];
            }
        }
        
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();      
    }

    public function inserePermissao()
    {
        $registroId = $_POST['registroId'];
        $grupoId = $_POST['grupoId'];
        $descricao = $_POST['descricao'];
        $ativo = isset($_POST['ativo']) ? 1 : 0;
        
        if ($registroId > 0) {
            $date_at = 'updated_at';
        } else {
            $date_at = 'created_at';
        } 

        $request = [
            'descricao' => $descricao,
            $date_at => date("Y-m-d H:i:s")
        ];

        if ($registroId > 0) {
            $result = $this->permissaoModel->editarPermissao($registroId, $request);
        } else {
            $result = $this->permissaoModel->inserirPermissao($request);
        }        

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();     
    }

    public function recuperaPermissao()
    {
        $id = $_GET['id'];
        $permissao = $this->permissaoModel->recuperarPermissao($id);
        echo json_encode($permissao);
        return $permissao;        
    }

    public function excluiPermissao()
    {
        $id = $_GET['id'];
        $grupoId = $_GET['grupoId'];
        
        $result = $this->permissaoModel->excluirPermissao($id);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit(); 
    }

    public function habilitarPermissao()
    {
        $id = $_GET['id'];
        $grupoId = $_GET['grupoId'];
        $value = 1;
        
        $result = $this->permissaoModel->habilitarDesabilitarPermissao($id, $value);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit(); 
    }

    public function desabilitarPermissao()
    {
        $id = $_GET['id'];
        $grupoId = $_GET['grupoId'];
        $value = 0;
                
        $result = $this->permissaoModel->habilitarDesabilitarPermissao($id, $value);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit(); 
    }

    public function verificacao($permissionId, $grupoId) {
        return $this->grupoPermissaoModel->verificaPermissaoGrupo($permissionId, $grupoId);
    }
}
