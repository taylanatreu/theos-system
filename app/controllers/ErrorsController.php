<?php

namespace App\Controllers;

require_once __DIR__ . '/Controller.php';

class ErrorsController extends Controller
{
    public function notfound()
    {
        include '../app/views/errors/404.php';
    }


    public function unauthorized()
    {
        include '../app/views/errors/402.php';
    }
}
