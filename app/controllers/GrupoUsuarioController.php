<?php

namespace App\Controllers;

use Databases\Database;
use PDO;
use App\Models\GlobalsModel;
use App\Models\GrupoUsuarioModel;
use App\Models\GrupoModel;
use App\Models\UsuarioModel;

require_once __DIR__ . '/Controller.php';

class GrupoUsuarioController extends Controller
{
    private $grupoUsuarioModel;
    private $globalsModel;

    public function __construct(Database $database)
    {
        parent::__construct($database);
        $this->globalsModel = new GlobalsModel();
        $this->grupoUsuarioModel = new GrupoUsuarioModel($this->database, $this->globalsModel);
        $this->grupoModel = new GrupoModel($this->database, $this->globalsModel);
        $this->usuarioModel = new UsuarioModel($this->database, $this->globalsModel);
    }

    public function index()
    { 
        $grupoId = $_GET["grupoId"];
        $grupo = $this->grupoModel->recuperarGrupo($grupoId);
        
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->usuarioModel->contaTodosRegistrosAtivosUsuario();
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;
        $usuariosAtivos = $this->usuarioModel->recuperarTodosPaginadoAtivosUsuario($registrosPorPagina, $offset);

        include '../app/views/seguranca/lista-grupos-usuarios.php';
    }

    public function pesquisa()
    {
        $grupoId = $_GET["grupoId"];
        $grupo = $this->grupoModel->recuperarGrupo($grupoId);
        
        $termoBusca = isset($_GET['termo_busca']) ? $_GET['termo_busca'] : '';
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->usuarioModel->contaRegistrosBuscadosUsuario($termoBusca);
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;

        $result = $this->usuarioModel->buscarRegistrosUsuario($termoBusca, $registrosPorPagina, $offset);

        $html = '';
        $x = 1;

        if (isset($result) && is_array($result) && !empty($result)) {
            foreach ($result as $resultado) {

                $checked = $this->verificacao($resultado->id, $grupoId) ? 'checked' : '';

                $html .= '<tr>';
                $html .= '<td class="text-center">' . $x . '</td>';
                $html .= '<td class="text-center">' . $resultado->nome . ' ' .  $resultado->sobrenome .'</td>';
                $html .= '<td class="text-center">' . $resultado->email . '</td>';
                $html .= '<td class="text-center">
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-off-default custom-switch-on-success">
                                    <input 
                                        type="checkbox" 
                                        class="custom-control-input usuario-switch" 
                                        id="usuario-' . $resultado->id . '"
                                        data-user-id="' . $resultado->id . '" 
                                        data-grupo-id="' . $grupoId . '"
                                        ' .  $checked . '>
                                    <label class="custom-control-label" for="usuario-' . $resultado->id . '"></label>
                                </div>
                            </div>
                        </td>';
                $html .= '</tr>';
                $x ++; 
            } 
        } else {
            $html .= '<tr>';
            $html .= '<td class="text-center" colspan="4">Nenhum registro encontrado.</td>';
            $html .= '</tr>';
        }

        $htmlPagination = '';
        $htmlPagination .= 'Exibindo ' . $registrosPorPagina . ' registros da página ' . $paginaAtual . '/' . $totalPaginas . ' de um total de ' . $totalRegistros . ' registros.'; 

        $response = [
            "dados" => $html,
            'pagination' => $htmlPagination
        ];       
        
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();   
    }

    public function insere()
    {
        //$registroId = $_POST["registroId"];
        $grupoId = $_POST['grupoId'];
        $userId = $_POST['itemId'];
        
        $request = [
            'grupoId' => $grupoId,
            'userId' => $userId
        ];

        if ($this->verificacao($userId, $grupoId)) {
            $results = $this->grupoUsuarioModel->removerGrupoUsuario($request);
        } else {
            $results = $this->grupoUsuarioModel->inserirGrupoUsuario($request);
        }
        
        header('Content-Type: application/json');
        echo json_encode($results);
        exit();      
    }

    public function verificacao($userId, $grupoId) {
        return $this->grupoUsuarioModel->verificaUsuario($userId, $grupoId);
    }

    public function recuperaTodosPorUsuario($userId)
    {
        return $this->grupoUsuarioModel->recuperarTodosGrupoUsuarioPorUsuario($userId);

    }
    
}
