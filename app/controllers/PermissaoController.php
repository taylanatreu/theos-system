<?php

namespace App\Controllers;

use Databases\Database;
use PDO;
use App\Models\PermissaoModel;

require_once __DIR__ . '/Controller.php';

class PermissaoController extends Controller
{
    private $permissaoModel;

    public function __construct(Database $database)
    {
        parent::__construct($database);
        $this->permissaoModel = new PermissaoModel($this->database);
    }

    public function insere()
    {
        $grupoId = $_POST['grupoId'];
        $descricao = $_POST['descricao'];
        $ativo = isset($_POST['ativo']) ? 1 : 0;
        
        $request = [
            'descricao' => $descricao,
            'created_at' => date("Y-m-d H:i:s")
        ];

        $result = $this->permissaoModel->inserirPermissao($request);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit(); 
         
    }
}
