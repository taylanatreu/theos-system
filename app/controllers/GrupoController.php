<?php

namespace App\Controllers;

use Databases\Database;
use PDO;
use App\Models\GlobalsModel;
use App\Models\GrupoModel;

require_once __DIR__ . '/Controller.php';

class GrupoController extends Controller
{
    private $grupoModel;
    private $globalsModel;

    public function __construct(Database $database)
    {
        parent::__construct($database);
        $this->globalsModel = new GlobalsModel();
        $this->grupoModel = new GrupoModel($this->database, $this->globalsModel);
    }

    public function index()
    {
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->grupoModel->contaTodosRegistrosGrupo();
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;
        $grupos = $this->grupoModel->recuperarTodosPaginadoGrupo($registrosPorPagina, $offset);

        include '../app/views/seguranca/lista-grupos.php';
    }

    public function refresh()
    {
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->grupoModel->contaTodosRegistrosGrupo();
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;
        $grupos = $this->grupoModel->recuperarTodosPaginadoGrupo($registrosPorPagina, $offset);

        $html = '';
        $x = 1;
        if (isset($grupos) && is_array($grupos) && !empty($grupos)) {
            foreach ($grupos as $grupo) {

                if ($grupo->ativo == 1) {
                    $status = '<i class="far fa-check-square"></i>';
                    $target = '#confirmacaoDesabilitaModal';
                    $title = 'Desabilitar';
                    $icon = 'fas fa-ban';
                    $class = 'warning';
                } else {
                    $status = '<i class="fas fa-ban"></i>';
                    $target = '#confirmacaoHabilitaModal';
                    $title = 'Habilitar';
                    $icon = 'fas fa-check';
                    $class = 'success';
                }

                $html .= '<tr id="registro'. $grupo->id .'">';
                $html .= '<td class="text-center">' . $x . '</td>';
                $html .= '<td class="text-center">' . $grupo->name . '</td>';
                $html .= '<td class="text-center">' . $grupo->descricao . '</td>';
                $html .= '<td class="text-center">' . $status . '</td>';
                $html .= '<td class="text-center">
                            <button 
                                type="button" 
                                class="btn btn-xs btn-info" 
                                title="Editar Grupo" 
                                onclick="editarRegistro(' . $grupo->id . ')">
                                <i class="fas fa-edit"></i>
                            </button> 

                            <a 
                                href="#" 
                                class="btn btn-xs btn-'. $class .'" 
                                data-toggle="modal" 
                                data-target="'. $target . '" 
                                data-id="' . $grupo->id . '"
                                title="'. $title .' Grupo">
                                <i class="'. $icon . '"></i>
                            </a>

                            <a 
                                href="#" 
                                class="btn btn-xs btn-danger" 
                                title="Remover Grupo" 
                                data-toggle="modal" 
                                data-target="#confirmacaoExclusaoModal" 
                                data-id="' . $grupo->id . '">
                                <i class="fas fa-trash"></i>
                            </a>
                        </td>';
                $html .= '<td class="text-center">
                            <a 
                                href="/grupos-usuarios?grupoId=' . $grupo->id . '" 
                                class="btn btn-xs btn-dark link" 
                                title="Visualizar Usuários">
                                <i class="fas fa-users"></i> Usuários
                            </a>
                            <a 
                                href="/grupos-permissoes?grupoId=' . $grupo->id . '" 
                                class="btn btn-xs btn-success link" 
                                title="Visualizar Permissões">
                                <i class="fas fa-users"></i> Permissões
                            </a>
                        </td>';
                $html .= '</tr>';
                $x ++; 
            } 
        } else {
            $html .= '<tr>';
            $html .= '<td class="text-center" colspan="6">Nenhum registro encontrado.</td>';
            $html .= '</tr>';
        }        
        
        $htmlPagination = '';
        $htmlPagination .= 'Exibindo ' . $registrosPorPagina . ' registros da página ' . $paginaAtual . '/' . $totalPaginas . ' de um total de ' . $totalRegistros . ' registros.'; 

        $response = [
            "dados" => $html,
            'pagination' => $htmlPagination
        ];
        
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();   
    }

    public function pesquisa()
    {
        $termoBusca = isset($_GET['termo_busca']) ? $_GET['termo_busca'] : '';
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->grupoModel->contaRegistrosBuscadosGrupo($termoBusca);
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;

        $grupos = $this->grupoModel->buscarRegistrosGrupo($termoBusca, $registrosPorPagina, $offset);

        $html = '';
        $x = 1;

        if (isset($grupos) && is_array($grupos) && !empty($grupos)) {
            foreach ($grupos as $grupo) {

                if ($grupo->ativo == 1) {
                    $status = '<i class="far fa-check-square"></i>';
                    $target = '#confirmacaoDesabilitaModal';
                    $title = 'Desabilitar';
                    $icon = 'fas fa-ban';
                    $class = 'warning';
                } else {
                    $status = '<i class="fas fa-ban"></i>';
                    $target = '#confirmacaoHabilitaModal';
                    $title = 'Habilitar';
                    $icon = 'fas fa-check';
                    $class = 'success';
                }

                $html .= '<tr id="registro'. $grupo->id .'">';
                $html .= '<td class="text-center">' . $x . '</td>';
                $html .= '<td class="text-center">' . $grupo->name . '</td>';
                $html .= '<td class="text-center">' . $grupo->descricao . '</td>';
                $html .= '<td class="text-center">' . $status . '</td>';
                $html .= '<td class="text-center">
                            <button 
                                type="button" 
                                class="btn btn-xs btn-info" 
                                title="Editar Grupo" 
                                onclick="editarRegistro(' . $grupo->id . ')">
                                <i class="fas fa-edit"></i>
                            </button> 

                            <a 
                                href="#" 
                                class="btn btn-xs btn-'. $class .'" 
                                data-toggle="modal" 
                                data-target="'. $target . '" 
                                data-id="' . $grupo->id . '"
                                title="'. $title .' Grupo">
                                <i class="'. $icon . '"></i>
                            </a>

                            <a 
                                href="#" 
                                class="btn btn-xs btn-danger" 
                                title="Remover Grupo" 
                                data-toggle="modal" 
                                data-target="#confirmacaoExclusaoModal" 
                                data-id="' . $grupo->id . '">
                                <i class="fas fa-trash"></i>
                            </a>
                        </td>';
                $html .= '<td class="text-center">
                            <a 
                                href="/grupos-usuarios?grupoId=' . $grupo->id . '" 
                                class="btn btn-xs btn-dark link" 
                                title="Visualizar Usuários">
                                <i class="fas fa-users"></i> Usuários
                            </a>
                            <a 
                                href="/grupos-permissoes?grupoId=' . $grupo->id . '" 
                                class="btn btn-xs btn-success link" 
                                title="Visualizar Permissões">
                                <i class="fas fa-users"></i> Permissões
                            </a>
                        </td>';
                $html .= '</tr>';
                $x ++; 
            } 
        } else {
            $html .= '<tr>';
            $html .= '<td class="text-center" colspan="6">Nenhum registro encontrado.</td>';
            $html .= '</tr>';
        }

        $htmlPagination = '';
        $htmlPagination .= 'Exibindo ' . $registrosPorPagina . ' registros da página ' . $paginaAtual . '/' . $totalPaginas . ' de um total de ' . $totalRegistros . ' registros.'; 

        $response = [
            "dados" => $html,
            'pagination' => $htmlPagination
        ];
       
        
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();   
    }

    public function insere()
    {
        $registroId = $_POST['registroId'];
        $name = $_POST['name'];
        $descricao = $_POST['descricao'];
        $ativo = isset($_POST['ativo']) ? 1 : 0;

        if ($registroId > 0) {
            $date_at = 'updated_at';
        } else {
            $date_at = 'created_at';
        } 

        $request = [
            'name' => $name,
            'descricao' => $descricao,
            'ativo' => $ativo,
            $date_at => date("Y-m-d H:i:s")
        ];

        if ($registroId > 0) {
            $result = $this->grupoModel->editarGrupo($registroId, $request);
        } else {
            $result = $this->grupoModel->inserirGrupo($request);
        }        

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();        
    }

    public function recupera()
    {
        $id = $_GET['id'];
        $result = $this->grupoModel->recuperarGrupo($id);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();     
    }

    public function exclui()
    {
        $id = $_GET['id'];        
        $result = $this->grupoModel->excluirGrupo($id);
        
        header('Content-Type: application/json');
        echo json_encode($result);
        exit();        
    }

    public function habilitar()
    {
        $id = $_GET['id'];
        $value = 1;
        
        $result = $this->grupoModel->habilitarDesabilitarGrupo($id, $value);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    public function desabilitar()
    {
        $id = $_GET['id'];
        $value = 0;
                
        $result = $this->grupoModel->habilitarDesabilitarGrupo($id, $value);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }
}
