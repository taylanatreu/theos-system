<?php

namespace App\Controllers;

use Databases\Database;
use PDO;
use App\Models\GlobalsModel;
use App\Models\UsuarioModel;

require_once __DIR__ . '/Controller.php';

class UsuarioController extends Controller
{
    private $usuarioModel;
    private $globalsModel;

    public function __construct(Database $database)
    {
        parent::__construct($database);
        $this->globalsModel = new GlobalsModel();
        $this->usuarioModel = new UsuarioModel($database, $this->globalsModel);
    }

    public function index()
    {
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->usuarioModel->contaTodosRegistrosUsuario();
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;
        $users = $this->usuarioModel->recuperarTodosPaginadoUsuario($registrosPorPagina, $offset);
        
        include '../app/views/usuarios/lista-usuarios.php';
    }

    public function refresh()
    {
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->usuarioModel->contaTodosRegistrosUsuario();
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;
        $results = $this->usuarioModel->recuperarTodosPaginadoUsuario($registrosPorPagina, $offset);

        $html = '';
        $x = 1;

        if (isset($results) && is_array($results) && !empty($results)) {
            foreach ($results as $resultado) {

                if ($resultado->ativo == 1) {
                    $status = '<i class="far fa-check-square"></i>';
                    $target = '#confirmacaoDesabilitaModal';
                    $title = 'Desabilitar';
                    $icon = 'fas fa-ban';
                    $class = 'warning';
                } else {
                    $status = '<i class="fas fa-ban"></i>';
                    $target = '#confirmacaoHabilitaModal';
                    $title = 'Habilitar';
                    $icon = 'fas fa-check';
                    $class = 'success';
                }

                $created_at = date("d/m/Y H:i", strtotime($resultado->created_at));

                if ($resultado->avatar) {
                    $imagem = '<img 
                                class="img-fluid img-circle"
                                width="50"
                                src="/storage/users/' . $resultado->avatar . '"
                                alt="' . $resultado->nome . ' ' . $resultado->sobrenome . '">';
                } else  {
                    $imagem = '<i class="fas fa-user-circle fa-2x"></i>';   
                }

                $html .= '<tr>';
                $html .= '<td class="text-center" style="width: 10%;">' . $x . '</td>';
                $html .= '<td class="text-center">
                            <div>' . $imagem .'</div>
                            <div><strong> ' . $resultado->nome . ' ' . $resultado->sobrenome . '</strong></div>
                          </td>';
                $html .= '<td class="text-center">' . $resultado->email . '</td>';
                $html .= '<td class="text-center">' . $status . '</td>';
                $html .= '<td class="text-center">' . $created_at . '</td>';
                $html .= '<td class="text-center">
                            <button 
                                type="button" 
                                class="btn btn-xs btn-info" 
                                title="Editar Usuário" 
                                onclick="editarRegistro(' . $resultado->id . ')">
                                <i class="fas fa-edit"></i>
                            </button> 

                            <a 
                                href="#" 
                                class="btn btn-xs btn-'. $class .'" 
                                data-toggle="modal" 
                                data-target="'. $target . '" 
                                data-id="' . $resultado->id . '"
                                title="'. $title .' Usuário">
                                <i class="'. $icon . '"></i>
                            </a>
                        </td>';
                $html .= '</tr>';
                $x ++; 
            } 
        } else {
            $html .= '<tr>';
            $html .= '<td class="text-center" colspan="6">Nenhum registro encontrado.</td>';
            $html .= '</tr>';
        }

        $htmlPagination = '';
        $htmlPagination .= 'Exibindo ' . $registrosPorPagina . ' registros da página ' . $paginaAtual . '/' . $totalPaginas . ' de um total de ' . $totalRegistros . ' registros.'; 

        $response = [
            "dados" => $html,
            'pagination' => $htmlPagination
        ];
       
        
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();
    }

    public function pesquisa()
    {
        $termoBusca = isset($_GET['termo_busca']) ? $_GET['termo_busca'] : '';
        $registrosPorPagina = 50;
        $paginaAtual = isset($_GET['page']) ? $_GET['page'] : 1;
        $paginaAtual = max(1, $paginaAtual);

        $totalRegistros = $this->usuarioModel->contaRegistrosBuscadosUsuario($termoBusca);
        $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
        $offset = ($paginaAtual - 1) * $registrosPorPagina;
        $results = $this->usuarioModel->buscarRegistrosUsuario($termoBusca, $registrosPorPagina, $offset);

        $html = '';
        $x = 1;

        if (isset($results) && is_array($results) && !empty($results)) {
            foreach ($results as $resultado) {

                if ($resultado->ativo == 1) {
                    $status = '<i class="far fa-check-square"></i>';
                    $target = '#confirmacaoDesabilitaModal';
                    $title = 'Desabilitar';
                    $icon = 'fas fa-ban';
                    $class = 'warning';
                } else {
                    $status = '<i class="fas fa-ban"></i>';
                    $target = '#confirmacaoHabilitaModal';
                    $title = 'Habilitar';
                    $icon = 'fas fa-check';
                    $class = 'success';
                }

                $created_at = date("d/m/Y H:i", strtotime($resultado->created_at));

                if ($resultado->avatar) {
                    $imagem = '<img 
                                class="img-fluid img-circle"
                                width="50"
                                src="/storage/users/' . $resultado->avatar . '"
                                alt="' . $resultado->nome . ' ' . $resultado->sobrenome . '">';
                } else  {
                    $imagem = '<i class="fas fa-user-circle fa-2x"></i>';   
                }

                $html .= '<tr>';
                $html .= '<td class="text-center" style="width: 10%;">' . $x . '</td>';
                $html .= '<td class="text-center">
                            <div>' . $imagem .'</div>
                            <div><strong> ' . $resultado->nome . ' ' . $resultado->sobrenome . '</strong></div>
                          </td>';
                $html .= '<td class="text-center">' . $resultado->email . '</td>';
                $html .= '<td class="text-center">' . $status . '</td>';
                $html .= '<td class="text-center">' . $created_at . '</td>';
                $html .= '<td class="text-center">
                            <button 
                                type="button" 
                                class="btn btn-xs btn-info" 
                                title="Editar Usuário" 
                                onclick="editarRegistro(' . $resultado->id . ')">
                                <i class="fas fa-edit"></i>
                            </button> 

                            <a 
                                href="#" 
                                class="btn btn-xs btn-'. $class .'" 
                                data-toggle="modal" 
                                data-target="'. $target . '" 
                                data-id="' . $resultado->id . '"
                                title="'. $title .' Usuário">
                                <i class="'. $icon . '"></i>
                            </a>
                        </td>';
                $html .= '</tr>';
                $x ++; 
            } 
        } else {
            $html .= '<tr>';
            $html .= '<td class="text-center" colspan="6">Nenhum registro encontrado.</td>';
            $html .= '</tr>';
        }

        $htmlPagination = '';
        $htmlPagination .= 'Exibindo ' . $registrosPorPagina . ' registros da página ' . $paginaAtual . '/' . $totalPaginas . ' de um total de ' . $totalRegistros . ' registros.'; 

        $response = [
            "dados" => $html,
            'pagination' => $htmlPagination
        ];

        header('Content-Type: application/json');
        echo json_encode($response);
        exit();
    }

    public function insere()
    {
        $registroId = $_POST['registroId'];
        $nome = $_POST["nome"];
        $sobrenome = $_POST["sobrenome"];
        $email = $_POST["email"];
        
        $senha = $_POST["senha"];
        $senha2 = $_POST["confirmaSenha"];
        $ativo = isset($_POST['ativo']) ? 1 : 0;

        if ($registroId > 0) {
            $usuario = $this->usuarioModel->recuperaUsuario($registroId);
            $date_at = 'updated_at';
        } else {
            $date_at = 'created_at';
        }

        if ($senha != $senha2) {
            $result = [
                'success' => false,
                'message' => 'Você digitou duas senhas diferentes.',
                'title' => 'Falha'
            ];
        } else {
            $request = [
                'user_type' => 1,
                'nome' => $nome,
                'sobrenome' => $sobrenome,
                'email' => $email,
                'senha' => $senha,
                'ativo' => $ativo,
                $date_at => date("Y-m-d H:i:s")
            ];

            if ($registroId > 0) {
                $result = $this->usuarioModel->editarUsuario($registroId, $request);
            } else {
                $result = $this->usuarioModel->inserirUsuario($request);
            }
        }

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    public function editaProfile()
    {
        $idEditar = $_SESSION["user_id"];
        $nome = $_POST["nome"];
        $sobrenome = $_POST["sobrenome"];
        $email = $_POST["email"];

        $request = [
            'nome' => $nome,
            'sobrenome' => $sobrenome,
            'email' => $email,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $result = $this->usuarioModel->editarProfileUsuario($idEditar, $request);
        
        header('Content-Type: application/json');
        echo json_encode($result);
        exit(); 
    }

    public function recuperaUsuario($id)
    {
        $result = $this->usuarioModel->recuperaUsuario($id);

        return $result;
    }

    public function recupera()
    {
        $id = $_GET['id'];
        $result = $this->usuarioModel->recuperaUsuario($id);
        
        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    public function avatar()
    {
        $userId = $_SESSION["user_id"];
        $profile = $this->recuperaUsuario($userId);

        if (isset($_FILES['imagem']) && $_FILES['imagem']['error'] === UPLOAD_ERR_OK) {
            $tamanhoMaximo = 6 * 1024 * 1024; 
            if ($_FILES['imagem']['size'] > $tamanhoMaximo) {
                $success = false;
                $msg = "O tamanho da imagem excede o limite de 6MB.";
                $title = "Falha no Upload";
            }

            $extensoesPermitidas = ['jpg', 'jpeg', 'png', 'gif'];
            $extensaoArquivo = strtolower(pathinfo($_FILES['imagem']['name'], PATHINFO_EXTENSION));
            
            if (!in_array($extensaoArquivo, $extensoesPermitidas)) {
                $success = false;
                $msg = "A extensão do arquivo não é permitida.";
                $title = "Falha no Upload";
            }

            $caminho = $this->salvarImagemNoServidor($_FILES['imagem']);

            if ($caminho) {
                $caminhoBase = realpath("../public/storage/users");
                $caminhoAntigo = $caminhoBase . DIRECTORY_SEPARATOR . urlencode($this->bancoModel->obterCaminhoImagemUsuario($userId));

                if ($caminhoAntigo && file_exists($caminhoAntigo)) {
                    unlink($caminhoAntigo);
                }

                if ($this->usuarioModel->salvarImagemNoBanco($userId, $caminho)) {
                    $success = true;
                    $msg = "O upload da imagem foi realizado com sucesso.";
                    $title = "Sucesso";
                }                
                
            } else {
                $success = false;
                $msg = "Ocorreu um erro inesperado, tente novamente.";
                $title = "Falha no Upload";
            }

        } else {
            $success = false;
            $msg = "Você não selecionou nenhuma imagem.";
            $title = "Falha no Upload";
        }

        $result = [
            'success' => $success, 
            'message' => $msg, 
            'title' => $title,
            'imagem' => $caminho
        ];

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    private function salvarImagemNoServidor($imagem) {
        $diretorioDestino = '../public/storage/users/'; 

        $nomeArquivo = uniqid() . '_profile_user';

        $caminhoCompleto = $diretorioDestino . $nomeArquivo;

        if (move_uploaded_file($imagem['tmp_name'], $caminhoCompleto)) {
            return $nomeArquivo;
        } else {
            return false;
        }
    }

    public function alteraSenha()
    {
        $senha = $_POST["senha"];
        $senha2 = $_POST["confirmaSenha"];

        if ($senha != $senha2) {
            $result = [
                'success' => false,
                'message' => 'Você digitou duas senhas diferentes.',
                'title' => 'Falha'
            ];
        } else {
            $id = $_SESSION["user_id"];
        
            $usuario = $this->usuarioModel->recuperaUsuario($id);
            $senhaCodificada = password_hash($senha, PASSWORD_DEFAULT);
            
            $result = $this->usuarioModel->alterarSenhaUsuario($id, $senhaCodificada);
        }

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }
    
    public function exclui()
    {
        $id = $_GET['id'];
        
        $result = $this->usuarioModel->excluirUsuario($id);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    public function habilitar()
    {
        $id = $_GET['id'];
        $value = 1;
        
        $result = $this->usuarioModel->habilitarDesabilitarUsuario($id, $value);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    public function desabilitar()
    {
        $id = $_GET['id'];
        $value = 0;
                
        $result = $this->usuarioModel->habilitarDesabilitarUsuario($id, $value);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    public function profile()
    {
        $usuario = $this->usuarioModel->recuperaUsuario($_SESSION["user_id"]);
        include '../app/views/usuarios/profile.php';
        return $usuario;
    }

    public function verificarCookie($userId, $userToken)
    {
        $usuario = $this->usuarioModel->verificarCookie($userId, $userToken);
        
        return $usuario;
    }
}
