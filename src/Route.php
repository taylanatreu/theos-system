<?php

namespace Src;

class Route
{
    protected $method;
    protected $pattern;
    protected $callback;

    public function __construct($method, $pattern, $callback)
    {
        $this->method = $method;
        $this->pattern = $pattern;
        $this->callback = $callback;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    public function getCallback()
    {
        return $this->callback;
    }
}