<?php

namespace Src;

class Dispacher
{
    public function dispatch($controller, $action)
    {
        if (class_exists($controller)) {
            $objController = new $controller();
            if (method_exists($objController, $action)) {
                $objController->$action();
            } else {
                echo "Ação não encontrada!";
            }
        } else {
            echo "Controlador não encontrado!";
        }
    }
}
