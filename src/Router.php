<?php

namespace Src;

use Src\Dispacher;
use Src\RouteCollection;

class Router 
{
    protected $route_collection;
    protected $dispatcher;

    public function __construct()
    {
        $this->route_collection = new RouteCollection();
        $this->dispatcher = new Dispacher();
    }

    public function get($pattern, $callback)
    {
        $this->route_collection->add('get', $pattern, $callback);
        return $this;
    }

    public function post($pattern, $callback)
    {
        $this->route_collection->add('post', $pattern, $callback);
        return $this;   
    }

    public function put($pattern, $callback)
    {
        $this->route_collection->add('put', $pattern, $callback);
        return $this;   
    }

    public function delete($pattern, $callback)
    {
        $this->route_collection->add('delete', $pattern, $callback);
        return $this;   
    }

    public function find($request_type, $pattern)
    {
        $route = $this->route_collection->where($request_type, $pattern);

        if ($route) {
            return [
                'controller' => $route['callback'][0],
                'action' => $route['callback'][1],
                'params' => $route['uri']
            ];
        }

        return null;
    }


    public function group($prefix, callable $callback)
    {
        $callback($prefix);
    }

    public function getRoute($method, $url)
    {
        $urlParts = explode('?', $url);
        $path = $urlParts[0];

        $route = $this->route_collection->where($method, $path);

        if ($route && is_array($route->callback)) {
            return [
                'controller' => $route->callback[0],
                'action' => $route->callback[1],
                'params' => $route->uri
            ];
        }

        return null;
    }  
}
