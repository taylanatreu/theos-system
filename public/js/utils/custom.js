$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('.select2').select2();

    toastr.options = {
        timeOut: 3000,
        closeButton: true,
        progressBar: true
    };
});

function carregandoDados() {
    setTimeout(function () {
        var overlay = document.getElementById('overlay');
        if (overlay) {
            overlay.style.display = 'none';
        }
    }, 3000);
}

function atualizarTabela(grupoId) {
    var urlRefresh = construirUrl('refresh') + (grupoId ? '?grupoId=' + grupoId : '');
    $.ajax({
        url: urlRefresh,
        method: 'GET',
        dataType: 'json',
        success: function (response) {
            $('#registros').html(response.dados);
            $('#pagination').html(response.pagination);

            limparFormCadastro(
                'formAuth',
                'formCadastro',
                'formCliente',
            );
        },
        error: function (error) {
            toastr.error('O resultado da requisição é: ' + error + ' e o sistema espera uma resposta json', 'Erro do Resultado AJAX');
        }
    });
}

$(document).ready(function () {
    $('#cep').on('blur', function () {
        var cep = $(this).val().replace(/\D/g, '');

        if (!cep) {
            limparCamposEndereco(); 
        }


        if (cep.length === 8) {
            var cepValido = /^\d{8}$/.test(cep);

            if (cepValido) {
                $.ajax({
                    url: 'https://viacep.com.br/ws/' + cep + '/json/',
                    dataType: 'json',
                    success: function (data) {
                        if (!data.erro) {
                            $('#logradouro').val(data.logradouro);
                            $('#bairro').val(data.bairro);
                            $('#cidade').val(data.localidade);
                            $('#uf').val(data.uf);

                            $('#logradouroHiden').val(data.logradouro);
                            $('#bairroHiden').val(data.bairro);
                            $('#cidadeHiden').val(data.localidade);
                            $('#ufHiden').val(data.uf);

                            $('#numero').focus();
                        } else {
                            toastr.error("O CEP informado é inválido.", "Falha na requisição");
                            limparCamposEndereco();
                        }
                    },
                    error: function () {
                        toastr.error("O CEP informado é inválido.", "Falha na requisição");
                        limparCamposEndereco();
                    }
                });
            } else {
                limparCamposEndereco();
            }
        }
    });
});

function limparCamposEndereco() {
    $('#cep').val('');
    $('#logradouro').val('');
    $('#bairro').val('');
    $('#cidade').val('');
    $('#uf').val('');
    $('#numero').val('');
}

function limparFormCadastro(formId) {
    var formulario = document.getElementById(formId);
    var checkbox = document.getElementById('checkboxPrimary1');

    if (formulario) {
        var campos = formulario.querySelectorAll('input, textarea, select');
        campos.forEach(function (campo) {
            campo.value = '';
        });

        if (checkbox) {
            checkbox.checked = false;
        }
    }
}