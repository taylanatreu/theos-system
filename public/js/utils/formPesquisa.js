$(document).ready(function () {
    var exibirCampos = false;

    if (construirUrl('') === '/clientes') {
        exibirCamposAdicionais();
    }

    $('#formPesquisa').submit(function (event) {
        event.preventDefault();
        pesquisaSimples();
    });
    
    $('#formPesquisaAvancada').submit(function (event) {
        event.preventDefault();
        pesquisaAvancada();
    });

    function pesquisaSimples() {
        var termo_busca = $('#termoBuscaSimples').val();
        var grupoId = $('#grupoId').val();

        realizarPesquisa({ grupoId: grupoId, termo_busca: termo_busca });
    }

    function pesquisaAvancada() {
        var status = $('#fStatus').val();
        var classificacao = $('#fClassificacao').val();
        var order = $('#fOrder').val();
        var termo_busca = $('#termoBuscaAvancada').val();

        if (exibirCampos) {
            var tipoCliente = $('#fTipoCliente').val();
            var tipoEndereco = $('#fTipoEndereco').val();
            var tipoContato = $('#fTipoContato').val();

            realizarPesquisa({ 
                status: status, 
                classificacao: classificacao, 
                order: order, 
                termo_busca: termo_busca,
                tipoCliente: tipoCliente,
                tipoEndereco: tipoEndereco,
                tipoContato: tipoContato 
            });
        } else {
            realizarPesquisa({ 
                status: status, 
                classificacao: classificacao, 
                order: order, 
                termo_busca: termo_busca 
            });
        }
    }

    function realizarPesquisa(data) {
        $('#overlay').css('display', 'flex');

        $.ajax({
            url: construirUrl('busca'),
            method: 'GET',
            data: data,
            dataType: 'json',
            success: function (response) {
                carregandoDados();
                $('#registros').html(response.dados);
                $('#pagination').html(response.pagination);
                if (
                    data.status || data.classificacao || data.order ||
                    data.termo_busca || data.tipoCliente || data.tipoEndereco || data.tipoContato
                ) {
                    exibeFiltro(
                        data.status, data.classificacao, data.order, data.termo_busca,
                        response.numResultados, data.tipoCliente, data.tipoEndereco, data.tipoContato
                    );
                }
                
                $('#filtroPesquisaModal').modal('hide');
            },
            error: function (error) {
                toastr.error('O resultado da requisição é: ' + error + ' e o sistema espera uma resposta json', 'Erro do Resultado AJAX');

                // Em caso de erro, também esconde o modal
                $('#overlay').css('display', 'none');
            }
        });
    }

    function exibeFiltro(status, classificacao, order, termo_busca, numResultados, tipoCliente, tipoEndereco, tipoContato) {
        $('#resultadoPesquisa').empty();

        if (status === '1') {
            status = "Ativos";
        } else if (status === '0') {
            status = "Desabilitados";
        } else {
            status = "Todos";
        }

        if (numResultados > 1) {
            reg = "s";
        } else {
            reg = "";
        }

        if (tipoCliente === '1') {
            resTpCli = "Física";
        } else if (tipoCliente === '2') {
            resTpCli = "Jurídica";
        } else {
            resTpCli = "Todos";
        }

        if (tipoEndereco === '1') {
            resTpEnd = "Comercial";
        } else if (tipoEndereco === '0') {
            resTpEnd = "Residencial";
        } else {
            resTpEnd = "Todos";
        }

        if (tipoContato === '1') {
            resTpCont = "WhatsApp";
        } else {
            resTpCont = "Todos";
        }

        var pesquisaInfo = '<div><strong>Status:</strong> ' + status +
            ' / <strong>Classificação:</strong> ' + classificacao +
            ' / <strong>Ordem:</strong> ' + order +
            ' / <strong>Tipo de Pessoa:</strong> ' + resTpCli +
            ' / <strong>Tipo de Endereço:</strong> ' + resTpEnd +
            ' / <strong>Tipo de Contato:</strong> ' + resTpCont +
            (termo_busca && termo_busca.trim() !== "" ? ' <div><strong>Pesquisa:</strong> ' + termo_busca : '</div>') +
            '</div>';

        var resultadoInfo = '<div class="row">' +
            '<div class="col-md-6">' +
            '<p><strong>Resultado da Pesquisa:</strong> <i class="fas fa-search"></i> ' + numResultados + ' resultado' + reg + '</p>' +
            '</div>' +
            '<div class="col-md-6 text-right">' +
            '<a href="' + construirUrl('') + '" class="btn btn-xs btn-default link" title="Limpar Filtro">' +
            '<i class="fas fa-eraser"></i> Limpar Filtros' +
            '</a>' +
            '</div>' +
            '</div>';

        $('#resultadoPesquisa').append('<div class="card">' +
            '<div class="card-header">' +
            '<h3 class="card-title">' +
            '<i class="fas fa-filter"></i> Filtros Aplicados' +
            '</h3>' +
            '</div>' +
            '<div class="card-body">' +
            pesquisaInfo +
            '</div>' +
            '<div class="card-footer">' +
            resultadoInfo +
            '</div>' +
            '</div>');
    }

    function exibirCamposAdicionais() {
        $('#camposPersonalizados').show();
        exibirCampos = true;
    }
});
