$(document).ready(function () {    
    $('#formProfile').submit(function (event) {
        event.preventDefault();        
        var formData = $(this).serialize();

        $('#overlay').css('display', 'flex');

        $.ajax({
            url: construirUrl('edita'),
            method: 'POST',
            data: formData,
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    toastr.success(response.message, response.title);

                    var updated_at = moment(response.dados.updated_at).format('DD/MM/YYYY HH:mm');

                    $('#infoPerfil').text(response.dados.nome + " " + response.dados.sobrenome);
                    $('#infoProfile').text(response.dados.nome + " " + response.dados.sobrenome);
                    $('#emailProfile').text(response.dados.email);
                    $('#updatedProfile').text(updated_at);

                    carregandoDados();

                } else {
                    toastr.error(response.message, response.title);
                }
            },
            error: function (error) {
                toastr.error('Ocorreu um erro: ' + error, 'Falha no Processamento');
            },
            complete: function () {
                carregandoDados();
            }
        });
    });

    $('#formAvatar').submit(function (event) {
        event.preventDefault();
        
        var formData = new FormData(this);
    
        $('#overlay').css('display', 'flex');
    
        $.ajax({
            url: construirUrl('avatar'),
            method: 'POST',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.success) {
                    toastr.success(response.message, response.title);
                    $('#imgPerfil').attr('src', '/storage/users/' + response.imagem);
                    $('#avatarImg').attr('src', '/storage/users/' + response.imagem);
                    $('#imagem').val('');
                    carregandoDados();
                } else {
                    toastr.error(response.message, response.title);
                }
            },
            error: function (error) {
                toastr.error('O resultado da requisição é: ' + error + ' e o sistema espera uma resposta json', 'Erro do Resultado AJAX');
            },
            complete: function () {
                carregandoDados();
            }
        });
    });

    $('#formSenha').submit(function (event) {
        event.preventDefault();        
        var formData = $(this).serialize();

        $('#overlay').css('display', 'flex');

        $.ajax({
            url: construirUrl('altera-senha'),
            method: 'POST',
            data: formData,
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    toastr.success(response.message, response.title);
                    $('#senhaa').val('');
                    $('#confirmaSenha').val('');

                    carregandoDados();

                } else {
                    toastr.error(response.message, response.title);
                }
            },
            error: function (error) {
                toastr.error('Ocorreu um erro: ' + error, 'Falha no Processamento');
            },
            complete: function () {
                carregandoDados();
            }
        });
    });
});