$(document).ready(function () {  
    if (localStorage.getItem('naoExibirModal') === null) {
        localStorage.setItem('naoExibirModal', false);
    }

    if (construirUrl('') === "/dashboard" && localStorage.getItem('naoExibirModal') === 'false') {
        $('#BoasVindasModal').modal('show');
    }

    $('#ocultarMsg').on('change', function() {
        ocultarModal();
    });

    function ocultarModal() {
        if ($("#ocultarMsg").prop("checked")) {
            localStorage.setItem('naoExibirModal', true);
        }
        $('#BoasVindasModal').modal('hide');
    }
});
