$(document).ready(function () {
    var registroId;
    var grupoId;
    var grupo;

    function realizarAcao(url, sucessoMsg, titulo, modal) {
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('#' + modal).modal('hide');
                    toastr.success(response.message, sucessoMsg);
                    carregandoDados();
                    atualizarTabela(grupoId);
                } else {
                    toastr.error(response.message, titulo);
                }
            },
            error: function (error) {
                toastr.error('O resultado da requisição é: ' + error + ' e o sistema espera uma resposta json', 'Erro do Resultado AJAX');
            }
        });
    }

    // MODAL DE EXCLUSÃO DE REGISTRO
    $('#confirmacaoExclusaoModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        registroId = button.data('id');
        grupoId = button.data('grupo');
        if (grupoId) {
            grupo = "&grupoId=" + grupoId;
        } else {
            grupo = "";
        }
    });

    $('#confirmarExclusaoBtn').click(function () {
        var url = construirUrl('exclui') + '?id=' + registroId + grupo;
        realizarAcao(url, 'Registro excluído com sucesso', 'Erro na exclusão do registro', 'confirmacaoExclusaoModal');
    });

    // MODAL DE EXCLUSÃO DE IMAGEM
    $('#confirmacaoRemoverModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        registroId = button.data('id');
    });

    $('#confirmarRemoverBtn').click(function () {
        var url = construirUrl('removerImagem') + '?id=' + registroId;
        realizarAcao(url, 'Imagem removida com sucesso', 'Erro na remoção da imagem', 'confirmacaoRemoverModal');
    });

    // MODAL DE HABILITAÇÃO DE REGISTRO
    $('#confirmacaoHabilitaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        registroId = button.data('id');
        grupoId = button.data('grupo');
        if (grupoId) {
            grupo = "&grupoId=" + grupoId;
        } else {
            grupo = "";
        }
    });

    $('#confirmarHabilitarBtn').click(function () {
        var url = construirUrl('habilita') + '?id=' + registroId + grupo;
        realizarAcao(url, 'Registro habilitado com sucesso', 'Erro na habilitação do registro', 'confirmacaoHabilitaModal');
    });

    // MODAL DE DESABILITAÇÃO DE REGISTRO    
    $('#confirmacaoDesabilitaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        registroId = button.data('id');
        grupoId = button.data('grupo');
        if (grupoId) {
            grupo = "&grupoId=" + grupoId;
        } else {
            grupo = "";
        }
    });

    $('#confirmarDesabilitarBtn').click(function () {
        var url = construirUrl('desabilita') + '?id=' + registroId + grupo;
        realizarAcao(url, 'Registro desabilitado com sucesso', 'Erro na desabilitação do registro', 'confirmacaoDesabilitaModal');
    });
});