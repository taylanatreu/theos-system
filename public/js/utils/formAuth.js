$(document).ready(function () {  
    $('#formAuth').submit(function (event) {
        event.preventDefault();        
        var formData = new FormData(this);
        var url = '/dashboard';

        $.ajax({
            url: '/autenticacao',
            method: 'POST',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.success) {
                    toastr.success(response.message, response.title);
                    window.location.href = url;
                } else {
                    toastr.error(response.message, response.title);
                }
            },
            error: function (error) {
                toastr.error('O resultado da requisição é: ' + error + ' e o sistema espera uma resposta json', 'Erro do Resultado AJAX');
            }
        });
    });
});
