$(document).ready(function () {
    $('#registros').on('change', '.usuario-switch', function () {
        handleSwitchChange($(this), 'insere', 'user-id', 'Usuário');
    });
});

$(document).ready(function () {
    $('#registros').on('change', '.permissao-switch', function () {
        handleSwitchChange($(this), 'cadastra', 'permissao-id', 'Permissão');
    });
});

function handleSwitchChange(element, actionUrl, dataKey, titleKey) {
    var itemId = element.data(dataKey);
    var grupoId = element.data('grupo-id');
    var checked = element.prop('checked');

    $.ajax({
        url: construirUrl(actionUrl),
        method: 'POST',
        dataType: 'json',
        data: {
            itemId: itemId,
            grupoId: grupoId,
            checked: checked ? 1 : 0
        },
        success: function (response) {
            if (response.success) {
                toastr.success(response.message, response.title);
            } else {
                toastr.error(response.message, response.title);
            }
        },
        error: function (error) {
            toastr.error('O resultado da requisição é: ' + error + ' e o sistema espera uma resposta json', 'Erro do Resultado AJAX');
        }
    });
}