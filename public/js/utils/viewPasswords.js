$(document).ready(function () {
    $('#mostrarSenhaBtn, #mostrarConfirmaSenhaBtn').on('click', function () {
        var campoSenha = $(this).closest('.form-group').find('#senhaa, #confirmaSenha');
        var tipoAtual = campoSenha.prop('type') === 'password' ? 'text' : 'password';
        campoSenha.prop('type', tipoAtual);
        var icon = tipoAtual === 'password' ? 'far fa-eye' : 'far fa-eye-slash';
        $(this).find('i').attr('class', icon);
    });

    $('#viewPass').on('click', function () {
        var campoSenha = $(this).closest('.form-group').find('#senha');
        var tipoAtual = campoSenha.prop('type') === 'password' ? 'text' : 'password';
        campoSenha.prop('type', tipoAtual);
        var icon = tipoAtual === 'password' ? 'far fa-eye' : 'far fa-eye-slash';
        $(this).find('i').attr('class', icon);
    });
});