$(document).ready(function () {   
    var telefone = $('#telefone').text();
    var mascaraTelefone = telefone.length === 10 ? '(99) 9999-9999' : '(99) 99999-9999';
    $('#telefone').inputmask(mascaraTelefone);

    var celular = $('#celular').text();
    var mascaraCelular = celular.length === 10 ? '(99) 9999-9999' : '(99) 99999-9999';
    $('#celular').inputmask(mascaraCelular);

    var mascaraCep = '99999-999';
    $('#cep').inputmask(mascaraCep);

});