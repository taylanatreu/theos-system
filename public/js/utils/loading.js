$(document).ready(function () {
    setTimeout(function () {
        var loadPage = document.getElementById('loadPage');
        if (loadPage) {
            loadPage.style.display = 'none';
        }
    }, 3000);

    var overlay = $("#overlay");
    if (overlay.length) {
        overlay.show();
        setTimeout(function () {
            carregandoDados();
            overlay.hide();
        }, 3000);
    }
});
