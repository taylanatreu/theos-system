function formCadastro() {
    var novoBtn = $("#novoCadastro");
    var closeBtn = $("#closeBtn");
    var formularioCard = $("#cardCadastro");

    if (novoBtn.length > 0 && formularioCard.length > 0) {
        novoBtn.on('click', function(event) {
            event.preventDefault();
            showCard(formularioCard);
        });
    }

    closeBtn.on('click', function(event) {
        event.preventDefault();
        closeCard(formularioCard);
    });

    $('#formCadastro').submit(function (event) {
        event.preventDefault();

        var formData = new FormData(this);
        var grupoId = $('#grupoId').val();
        var url = construirUrl('insere');

        $('#overlay').css('display', 'flex');

        $.ajax({
            url: url,
            method: 'POST',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.success) {
                    toastr.success(response.message, response.title);
                    carregandoDados();
                    atualizarTabela(grupoId);
                    limparFormCadastro('formCadastro');
                    formularioCard.hide(); // Esconder o card após o sucesso
                } else {
                    toastr.error(response.message, response.title);
                }
            },
            error: function (error) {
                toastr.error('O resultado da requisição é: ' + error + ' e o sistema espera uma resposta json', 'Erro do Resultado AJAX');
            },
            complete: function () {
                $('#overlay').css('display', 'none');
            }
        });
    });

    // Função para preencher o formulário ao clicar em editarRegistro
    function preencherFormulario(dados) {
        // Preencher os campos comuns a todos os formulários
        $("#descricao").val(dados.descricao);
        $("#checkboxPrimary1").prop("checked", dados.ativo == 1);
        $("#registroId").val(dados.id);
    
        // Preencher os campos específicos de cada formulário
        if ($("#nome").length > 0) {
            // Preencher os campos do formulário de usuários
            $("#nome").val(dados.nome);
            $("#sobrenome").val(dados.sobrenome);
            $("#email").val(dados.email);
        }
    
        if ($("#name").length > 0) {
            // Preencher os campos do formulário de grupos
            $("#name").val(dados.name);
        }
    }

    // Função para editarRegistro (chamada ao clicar em editarRegistro)
    window.editarRegistro = function(registroId) {
        $.ajax({
            url: construirUrl('view') + '?id=' + registroId,
            method: 'GET',
            dataType: 'json',
            success: function (dados) {
                showCard(formularioCard);
                preencherFormulario(dados);
            },
            error: function (error) {
                toastr.error('O resultado da requisição é: ' + error + ' e o sistema espera uma resposta json', 'Erro do Resultado AJAX');
            }
        });
    };
}

// Chamar a função formCadastro() ao carregar o documento
$(document).ready(function () {
    formCadastro();
});


function closeCard(formularioCard) {
    $(formularioCard).fadeOut();
    setTimeout(function() {
        $(formularioCard).hide();
        limparFormCadastro('formCadastro');
    }, 500);
}

function showCard(formularioCard) {
    $(formularioCard).fadeIn();
    setTimeout(function() {
        $(formularioCard).show();
    }, 500);
}
