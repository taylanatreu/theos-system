function construirUrl(subrota) {
    var rotaAtual = window.location.pathname;
    rotaAtual = rotaAtual.replace(/^\//, '');

    if (subrota) {
        var subrota = '/' + subrota;
    }

    return '/' + rotaAtual + subrota;
}

function obterParam(parametro) {
    parametro = parametro.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + parametro + "=([^&#]*)");
    var resultados = regex.exec(location.search);
    return resultados === null ? "" : decodeURIComponent(resultados[1].replace(/\+/g, " "));
}

$(document).ready(function () {
    $('body').on('click', 'a.link', function (event) {
        event.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            url: construirUrl(''),
            method: 'GET',
            dataType: 'html',
            success: function (response) {
                window.location.href = url;
            },
            error: function (error) {
                toastr.error('O resultado da requisição é: ' + error + ' e o sistema espera uma resposta html', 'Erro do Resultado AJAX');
            },
        });
    });
});