$(document).ready(function () {

  validarCampos();
  validarMoments();

  // $("#formCliente").submit(function (event) {
  //   event.preventDefault();
  //   var formData = new FormData(this);

  //   $("#overlay").css("display", "flex");
  //   $.ajax({
  //     url: construirUrl("insere"),
  //     method: "POST",
  //     data: formData,
  //     dataType: "json",
  //     contentType: false,
  //     processData: false,
  //     success: function (response) {
  //       if (response.success) {
  //         toastr.success(response.message, response.title);
  //         limparFormCadastro("formCliente");
  //         setTimeout(function () {
  //           window.location.href = "/clientes";
  //         }, 2000);
  //       } else {
  //         toastr.error(response.message, response.title);
  //       }
  //     },
  //     error: function (error) {
  //       toastr.error(
  //         "O resultado da requisição é: " +
  //           error +
  //           " e o sistema espera uma resposta json",
  //           "Erro do Resultado AJAX"
  //       );
  //     },
  //     complete: function () {
  //       $("#overlay").css("display", "none");
  //     },
  //   });
  // });
});

function validarCampos() {
  var obrigatorios = $("#formCliente :input[required]");

  var preenchidos = true;

  obrigatorios.each(function () {
    if ($(this).val().trim() === "") {
      preenchidos = false;
      return false;
    }
  });
  
  if (preenchidos) {
    $("#btnSalvar").prop("disabled", false);
  } else {
    $("#btnSalvar").prop("disabled", true);
  }
  
  return {
    preenchidos: preenchidos
  };
}

function validarEmail(email) {
  var regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  return regexEmail.test(email);
}

function validarCPF(cpf) {
  cpf = cpf.replace(/[^\d]+/g, '');

  if (cpf.length !== 11 || /^(\d)\1{10}$/.test(cpf)) {
      return false; 
  }

  var add = 0;
  for (var i = 0; i < 9; i++) {
      add += parseInt(cpf.charAt(i)) * (10 - i);
  }
  var rev = 11 - (add % 11);
  if (rev === 10 || rev === 11) {
      rev = 0;
  }
  if (rev !== parseInt(cpf.charAt(9))) {
      return false;
  }

  add = 0;
  for (i = 0; i < 10; i++) {
      add += parseInt(cpf.charAt(i)) * (11 - i);
  }
  rev = 11 - (add % 11);
  if (rev === 10 || rev === 11) {
      rev = 0;
  }
  if (rev !== parseInt(cpf.charAt(10))) {
      return false;
  }

  return true;
}

function validarCNPJ(cnpj) {
  cnpj = cnpj.replace(/[^\d]+/g, '');

  if (cnpj.length !== 14 || /^(\d)\1{13}$/.test(cnpj)) {
      return false;
  }

  var add = 0;
  for (var i = 0; i < 12; i++) {
      add += parseInt(cnpj.charAt(i)) * (i < 4 ? 5 - i : 13 - i);
  }
  var rev = 11 - (add % 11);
  if (rev === 10 || rev === 11) {
      rev = 0;
  }
  if (rev !== parseInt(cnpj.charAt(12))) {
      return false;
  }

  add = 0;
  for (i = 0; i < 13; i++) {
      add += parseInt(cnpj.charAt(i)) * (i < 5 ? 6 - i : 14 - i);
  }
  rev = 11 - (add % 11);
  if (rev === 10 || rev === 11) {
      rev = 0;
  }
  if (rev !== parseInt(cnpj.charAt(13))) {
      return false;
  }

  return true;
}

function validarMoments() {
  $(document).ready(function () {
    $('#cpfCnpj').on('input', function () {
        var valorCampo = $(this).val().replace(/\D/g, '');
        var nomeCampo = this.id;
        var mascara;
        var valido;

        if (valorCampo.length === 11) {
            valido = validarCPF(valorCampo);

            if (valido) {
                mascara = '999.999.999-99';
            }
        } else if (valorCampo.length === 14) {
            valido = validarCNPJ(valorCampo);

            if (valido) {
                mascara = '99.999.999/9999-99';
            }
        }

        if (mascara) {
          verificarDado(nomeCampo, valorCampo);
          $(this).inputmask(mascara);
        } else {
          $(this).inputmask('');
        }
    });

    $('#cpfCnpj').on('paste', function () {
      // Aguarda um breve intervalo para que o valor seja colado completamente
      setTimeout(function () {
          $('#cpfCnpj').trigger('input'); // Dispara o evento de input para acionar a lógica de aplicação da máscara
      }, 100);
    });

    $("#email").blur(function () {
      var valorCampo = $(this).val();
      var nomeCampo = this.id;
      
      if (validarEmail(valorCampo)) {
          verificarDado(nomeCampo, valorCampo);
      } else {
          toastr.error("Você informou um Email inválido.", "EMAIL inválido!");
          $(this).val('').focus();
      }
    });

    $("#confirmaSenha").blur(function () {
      var senha = $("#senhaa").val();
      var confirmaSenha = $("#confirmaSenha").val();
      if (senha !== confirmaSenha) {
          toastr.error("Você digitou duas senhas diferentes.", "Senhas Diferentes!");
          $("#senhaa").val('').focus();
          $("#confirmaSenha").val('');
      }
    });
  });     
}

function verificarDado(nomeCampo, valorCampo) {
  $.ajax({
      url: construirUrl('view'),
      method: "POST",
      data: {
          nomeCampo: nomeCampo,
          valorCampo: valorCampo
      },
      dataType: 'json',
      success: function (response) {
          if (!response.success) {
              toastr.error(response.message, response.title);
              $("#" + nomeCampo).val('').focus();
              $(this).inputmask('remove');
          }
      },
      error: function (error) {
          toastr.error('O resultado da requisição é: ' + error + ' e o sistema espera uma resposta json', 'Erro do Resultado AJAX');
      }
  });
}