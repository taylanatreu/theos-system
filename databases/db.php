<?php

namespace Databases;

class Database
{    
    public function getPDO()
    {
        $dbHost = $_ENV["DB_HOST"];
        $dbDatabase = $_ENV["DB_DATABASE"];
        $dbUsername = $_ENV["DB_USERNAME"];
        $dbPassword = $_ENV["DB_PASSWORD"];
        $dbCharset = $_ENV["DB_CHARSET"];
        //$apiToken = $_ENV["API_TOKEN"];

        $dsn = sprintf(
            "mysql:host=%s;dbname=%s;charset=%s",
            $dbHost,
            $dbDatabase,
            $dbCharset
        );

        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            return new \PDO($dsn, $dbUsername, $dbPassword, $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    public function getAPIAccessToken()
    {
        return $_ENV["API_TOKEN"];
    }
}
