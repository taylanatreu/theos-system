<?php

require_once __DIR__ . '/databases/config.php';

session_start();
error_reporting(E_ALL);
ini_set('display_errors', true);

require_once    __DIR__ . '/databases/db.php';
require         __DIR__ . '/vendor/autoload.php';
require         __DIR__ . '/autoload.php';

use Databases\Database;
use Src\Router;

use App\Controllers\LoginController;
use App\Controllers\DashboardController;
use App\Controllers\UsuarioController;
use App\Controllers\ErrorsController;

use App\Controllers\GrupoController;
use App\Controllers\GrupoUsuarioController;
use App\Controllers\GrupoPermissaoController;

use App\Controllers\ClienteController;

use App\Controllers\LogController;

try {
    $database = new Database();

    $ctrLogin   = new LoginController($database);
    $ctrDash    = new DashboardController($database);
    $ctrUsuario = new UsuarioController($database);
    $ctrErrors  = new ErrorsController($database);

    $ctrGrupo   = new GrupoController($database);
    $ctrGpu     = new GrupoUsuarioController($database);
    $ctrGper    = new GrupoPermissaoController($database);    
    $ctrLog     = new LogController($database);
    
    $ctrCliente     = new ClienteController($database);

    $ctrlSessao = [
        'ctrLogin'      => $ctrLogin,
        'ctrDash'       => $ctrDash,
        'ctrUsuario'    => $ctrUsuario,
        'ctrErrors'     => $ctrErrors,
        'ctrGrupo'      => $ctrGrupo,
        'ctrGpu'        => $ctrGpu,
        'ctrGper'       => $ctrGper,
        'ctrLog'        => $ctrLog,
        'ctrCliente'        => $ctrCliente,
    ];

    $_SESSION['ctrlSessao'] = serialize($ctrlSessao);

    $router = new Router;
    require __DIR__ . '/routes/app.routing.php';

} catch (\Exception $e) {
    echo 'Error: ' . $e->getMessage();
    exit;
}
