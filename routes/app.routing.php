<?php

// AUTENTICAÇÃO
$router->get('/', function () {
    header('Location: /login');
    exit();
});

$router->get('/login', [$ctrLogin, 'index']);
$router->post('/autenticacao', [$ctrLogin, 'autenticacao']);
$router->get('/logout', [$ctrLogin, 'logout']);

// DASHBOARD
$router->get('/dashboard', [$ctrDash, 'index']);

// SEGURANÇA
$router->get('/grupos', [$ctrGrupo, 'index']);
$router->get('/grupos/busca', [$ctrGrupo, 'pesquisa']);
$router->get('/grupos/view', [$ctrGrupo, 'recupera']);
$router->post('/grupos/insere', [$ctrGrupo, 'insere']);
$router->post('/grupos/edita', [$ctrGrupo, 'edita']);
$router->get('/grupos/exclui', [$ctrGrupo, 'exclui']);
$router->get('/grupos/habilita', [$ctrGrupo, 'habilitar']);
$router->get('/grupos/desabilita', [$ctrGrupo, 'desabilitar']);
$router->get('/grupos/refresh', [$ctrGrupo, 'refresh']);

$router->get('/grupos-usuarios', [$ctrGpu, 'index']);
$router->post('/grupos-usuarios/insere', [$ctrGpu, 'insere']);
$router->get('/grupos-usuarios/busca', [$ctrGpu, 'pesquisa']);

$router->get('/grupos-permissoes', [$ctrGper, 'index']);
$router->get('/grupos-permissoes/busca', [$ctrGper, 'pesquisaPermissao']);
$router->get('/grupos-permissoes/view', [$ctrGper, 'recuperaPermissao']);
$router->post('/grupos-permissoes/cadastra', [$ctrGper, 'insere']);
$router->post('/grupos-permissoes/insere', [$ctrGper, 'inserePermissao']);
$router->post('/grupos-permissoes/edita', [$ctrGper, 'editaPermissao']);
$router->get('/grupos-permissoes/exclui', [$ctrGper, 'excluiPermissao']);
$router->get('/grupos-permissoes/habilita', [$ctrGper, 'habilitarPermissao']);
$router->get('/grupos-permissoes/desabilita', [$ctrGper, 'desabilitarPermissao']);
$router->get('/grupos-permissoes/refresh', [$ctrGper, 'refresh']);

// USUÁRIOS
$router->get('/usuarios', [$ctrUsuario, 'index']);
$router->post('/usuarios/insere', [$ctrUsuario, 'insere']);
$router->post('/usuarios/edita', [$ctrUsuario, 'edita']);
$router->get('/usuarios/view', [$ctrUsuario, 'recupera']);
$router->get('/usuarios/exclui', [$ctrUsuario, 'exclui']);
$router->get('/usuarios/habilita', [$ctrUsuario, 'habilitar']);
$router->get('/usuarios/desabilita', [$ctrUsuario, 'desabilitar']);
$router->get('/usuarios/busca', [$ctrUsuario, 'pesquisa']);
$router->get('/usuarios/refresh', [$ctrUsuario, 'refresh']);

$router->get('/profile', [$ctrUsuario, 'profile']);
$router->post('/profile/avatar', [$ctrUsuario, 'avatar']);
$router->post('/profile/altera-senha', [$ctrUsuario, 'alteraSenha']);
$router->post('/profile/edita', [$ctrUsuario, 'editaProfile']);

// CLIENTES
$router->get('/clientes', [$ctrCliente, 'index']);
$router->get('/clientes/create', [$ctrCliente, 'create']);
$router->post('/clientes/create/insere', [$ctrCliente, 'insere']);
$router->get('/clientes/details', [$ctrCliente, 'detalhes']);
$router->get('/clientes/refresh', [$ctrCliente, 'refresh']);
$router->get('/clientes/view', [$ctrCliente, 'recupera']);
$router->get('/clientes/exclui', [$ctrCliente, 'exclui']);
$router->get('/clientes/habilita', [$ctrCliente, 'habilitar']);
$router->get('/clientes/desabilita', [$ctrCliente, 'desabilitar']);
$router->get('/clientes/busca', [$ctrCliente, 'pesquisa']);

$router->post('/clientes/create/view', [$ctrCliente, 'buscaDados']);

// LOGS
$router->get('/historico-logs', [$ctrLog, 'index']);
$router->get('/historico-logs/busca', [$ctrLog, 'pesquisa']);

// ERRORS
$router->get('/404', [$ctrErrors, 'notfound']);
$router->get('/402', [$ctrErrors, 'unauthorized']);

$url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
$requestMethod = $_SERVER['REQUEST_METHOD'];

if ($url === '/' && $requestMethod === 'GET') {
    header('Location: /login');
    exit();
}

$route = $router->getRoute($requestMethod, $url);

if (is_array($route)) {
    $controller = $route['controller'];
    $action = $route['action'];
    
    call_user_func_array([new $controller($database), $action], []);
} else {    
    header("Location: /404");
    exit();
}
