<?php
spl_autoload_register(function ($class) {
    $baseDir = __DIR__ . "/app/controllers/";
    $classPath = str_replace('\\', '/', $class) . '.php';
    $namespace = str_replace($baseDir, '', $classPath);

    $nameController = strstr($namespace, 'Controllers/');
    
    if ($nameController !== false) {
        $nameController = str_replace('Controllers/', '', $nameController);

        $term = strstr($nameController, '/', true);

        if ($term !== false) {
            $filePath = $baseDir . $nameController;

            if (file_exists($filePath)) {
                require_once $filePath;
            }
        }
    }
});
